@extends('layouts.admin')
@section('content')

    <div class="content-wrap">
        <div class="main">

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Application</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')
                <div class="main-content">


                    <form class="form-horizontal" method="post" action="{{url('application/' . $application->apid. '/edit')}}">
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header">
                                        <h4> Student Data</h4>
                                        <div class="card-header-right-icon">
                                            <ul>
                                                <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                                <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="menu-upload-form">

                                            <div class="card-body">
                                                <div class="menu-upload-form">

                                                    <div class="form-group">
                                                        <label>Surname</label>
                                                        <input type="text" class="form-control"  name="surName" value="{{$application->surname}}" placeholder="Enter Surname">
                                                    </div>


                                                    <div class="form-group">
                                                        <label>Forenames (full names)</label>
                                                        <input type="text" placeholder="Enter Other Names in full" value="{{$application->forenames}}" name="foreNames" class="form-control">

                                                    </div>

                                                    <div class="form-group">
                                                        <label> Preferred Name</label>
                                                        <input type="text" name="preferredName" placeholder="Preferred Name" value="{{$application->preferredName}}" class="form-control" >

                                                    </div>



                                                    <div class="row">

                                                        <div class="form-group phone col-md-6">
                                                            <label for="gender">Gender</label>
                                                            <select class="form-control" name="gender">
                                                                <option selected disabled >Gender</option>
                                                                <option
                                                                        @if($application->gender == 'Male') selected @endif
                                                                        value="Male">Male</option>
                                                                <option
                                                                        @if($application->gender == 'Female') selected @endif
                                                                        value="Female">Female</option>
                                                            </select>
                                                        </div><!--//form-group-->


                                                        <div class="form-group email col-md-6">
                                                            <label for="email">Date of Birth<span class="required">*</span></label>
                                                            <input title="Date format should be DD-MM-YYYY" type="text" pattern="[0-9]{2}-[0-9]{2}-[0-9]{4}" value="{{$application->dob}}" name="dob" class="form-control bg-ash" placeholder="dd/mm/yyyy">

                                                        </div><!--//form-group-->

                                                    </div>


                                                    <div class="form-group forename">
                                                        <label for="email">Religion Domination<span class="required">*</span></label>
                                                        <input id="religion" name="religionDenomination" value="{{$application->religionDenomination}}" type="text" class="form-control" placeholder="Religion Domination">
                                                    </div><!--//form-group-->

                                                    <div class="form-group forename">
                                                        <label for="email">Nationality<span class="required">*</span></label>
                                                        <input id="religion" name="nationality" value="{{$application->nationality}}" type="text" class="form-control" placeholder="Nationality">
                                                    </div><!--//form-group-->



                                                    <div class="form-group message">
                                                        <label for="message">Home Address of child<span class="required">*</span></label>
                                                        <textarea id="message" class="form-control" rows="3" name="homeAddress"  placeholder="Home Address of child">{{$application->homeAddress}}</textarea>
                                                    </div><!--//form-group-->

                                                    <div class="form-group forename">
                                                        <label for="email">Home Telephone Number<span class="required">*</span></label>
                                                        <input id="religion" type="text" value="{{$application->homePhone}}" class="form-control" name="homePhone"  placeholder="Home Phone">
                                                    </div><!--//form-group-->


                                                    <div class="form-group forename">
                                                        <label for="email">First Language(language spoken at home)</label>
                                                        <input id="religion" type="text" class="form-control" value="{{$application->firstLanguage}}" name="firstLanguage"  placeholder="First Language">
                                                    </div><!--//form-group-->


                                                    <div class="form-group forename">
                                                        <label for="email">Proposed year of entry<span class="required">*</span></label>
                                                        <input id="religion" type="text" value="{{$application->proposedYear}}" class="form-control" name="proposedYear"  placeholder=" Proposed year Of Entry">
                                                    </div><!--//form-group-->


                                                </div>
                                            </div>




                                        </div>
                                    </div>
                                </div>
                                <!-- /# card -->
                            </div>
                            <!-- /# column -->
                        </div>

                        {{--current school--}}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header">
                                        <div class="card-header-right-icon">
                                            <ul>
                                                <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                                <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="menu-upload-form">
                                            <h3> Current School </h3>
                                            <br>
                                            <div class="form-group">
                                                <label>Current School</label>
                                                <input type="text" class="form-control" value="{{$application->currentSchool}}" name="currentSchool" placeholder="Current School Attended By Student And Address">
                                            </div>

                                            <div class="form-group">
                                                <label>Head teacher</label>
                                                <input type="text" class="form-control" value="{{$application->headTeacher}}"  name="headTeacher" placeholder="Head Teacher's Name (Include title e.g Dr,Mr,Mrs)" >
                                            </div>

                                            <div class="form-group">
                                                <label>Head teacher's Email </label>
                                                <input type="email" class="form-control" value="{{$application->headTeacherEmail}}" name="headTeacherEmail" placeholder="Head Teacher's Email" >
                                            </div>

                                            <div class="form-group">
                                                <label>School Phone</label>
                                                <input type="number" class="form-control" value="{{$application->schoolPhone}}" name="schoolPhone" placeholder="Phone Number of Someone at the school" >
                                            </div>
                                            {{--end form--}}
                                        </div>
                                    </div>
                                </div>
                                <!-- /# card -->
                            </div>
                            <!-- /# column -->
                        </div>



                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header">
                                        <div class="card-header-right-icon">
                                            <ul>
                                                <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                                <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="menu-upload-form">

                                            <hr>
                                            <h3> Father's Data </h3>
                                            <br>
                                            <div class="row">
                                                <div class="form-group phone col-md-6">
                                                    <label for="gender">Title</label>
                                                    <select class="form-control" name="fatherTitle">

                                                        <option selected>{{$application->fatherTitle}}</option>
                                                        <option>Mr.</option>
                                                        <option>Dr.</option>
                                                        <option>Eng.</option>
                                                        <option>Chief.</option>
                                                        <option>Barr.</option>
                                                    </select>
                                                </div><!--//form-group-->
                                                <div class="form-group email col-md-6">
                                                    <label for="email">Name<span class="required">*</span></label>
                                                    <input type="text" value="{{$application->father}}" name="father" class="form-control" placeholder=" Name of Father/Legal Guardian">
                                                </div><!--//form-group-->

                                            </div>

                                            <div class="form-group">
                                                <label>Fathers Profession </label>
                                                <input type="text" class="form-control" value="{{$application->fatherProfession}}" name="fatherProfession" placeholder="Father's Profession">
                                            </div>


                                            <div class="form-group">
                                                <label>Fathers Industry </label>
                                                <input type="text" class="form-control" value="{{$application->fatherIndustry}}" name="fatherIndustry" placeholder="Father's Industry" >
                                            </div>

                                            <div class="form-group">
                                                <label>Father Employers Name </label>
                                                <input type="text" class="form-control" value="{{$application->fatherEmployerName}}" name="fatherEmployerName" placeholder="Father Employers Name" >
                                            </div>

                                            <div class="form-group">
                                                <label>Fathers Email </label>
                                                <input type="text" class="form-control" value="{{$application->fatherEmail}}" name="fatherEmail" placeholder="Father's Email" >
                                            </div>


                                            <div class="form-group">
                                                <label>Fathers Phone </label>
                                                <input type="text" class="form-control" value="{{$application->fatherPhone}}" name="fatherPhone" placeholder="Fathers Phone Number">
                                            </div>


                                            {{--end form--}}
                                        </div>
                                    </div>
                                </div>
                                <!-- /# card -->
                            </div>
                            <!-- /# column -->
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header">
                                        <div class="card-header-right-icon">
                                            <ul>
                                                <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                                <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="menu-upload-form">


                                            <hr>
                                            <h3> Mother's Data </h3>
                                            <br>
                                            <div class="row">
                                                <div class="form-group phone col-md-6">
                                                    <label for="gender">Title</label>
                                                    <select class="form-control" name="motherTitle">
                                                        <option selected disabled > Title</option>
                                                        <option >Mrs.</option>
                                                        <option>Dr.</option>
                                                        <option>Eng.</option>
                                                        <option>Chief.</option>
                                                    </select>
                                                </div><!--//form-group-->
                                                <div class="form-group email col-md-6">
                                                    <label >Name<span class="required">*</span></label>
                                                    <input type="text" value="{{$application->mother}}" name="mother" class="form-control" placeholder="Name of Mother/Legal Guardian">
                                                </div><!--//form-group-->
                                            </div>
                                            <div class="form-group">
                                                <label>Mothers Profession </label>
                                                <input type="text" class="form-control" value="{{$application->motherProfession}}" name="motherProfession" placeholder="Mother's Profession" >
                                            </div>
                                            <div class="form-group">
                                                <label>Mothers Industry </label>
                                                <input type="text" class="form-control" value="{{$application->motherIndustry}}" name="motherIndustry" placeholder="Mother's Industry" >
                                            </div>
                                            <div class="form-group">
                                                <label>Mother Employers Name </label>
                                                <input type="text" class="form-control" value="{{$application->motherEmployerName}}" name="motherEmployerName" placeholder="Mother Employers Name" >
                                            </div>
                                            <div class="form-group">
                                                <label>Mother's Email </label>
                                                <input type="text" class="form-control" value="{{$application->motherEmail}}" name="motherEmail" placeholder="Mother's Email" >
                                            </div>
                                            <div class="form-group">
                                                <label>Mothers Phone </label>
                                                <input type="text" class="form-control" value="{{$application->motherPhone}}" name="motherPhone" placeholder="Mothers Phone Number">
                                            </div>



                                            {{--end form--}}
                                        </div>
                                    </div>
                                </div>
                                <!-- /# card -->
                            </div>
                            <!-- /# column -->
                        </div>


                        {{--Relatives--}}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header">
                                        <div class="card-header-right-icon">
                                            <ul>
                                                <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                                <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="menu-upload-form">

                                            {{--<form class="form-horizontal" method="post" action="{{url('post-create-application')}}">--}}
                                            {{--{{csrf_field()}}--}}
                                            <div class="card-body">
                                                <div class="menu-upload-form">
                                                    <hr>
                                                    <h3> Other People With Parental Responsibility </h3>
                                                    <br>
                                                    <div class="form-group">
                                                        <label>First Name </label>
                                                        <input type="text" class="form-control" value="{{$application->fname}}"  name="fname" placeholder="First Name" >
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Last Name </label>
                                                        <input type="text" class="form-control" value="{{$application->sname}}" name="sname" placeholder="First Name" >
                                                    </div>
                                                    <div class="form-group">
                                                        <label> Email</label>
                                                        <input type="email" class="form-control" value="{{$application->email}}" name="email" placeholder="Relative's Email" >
                                                    </div>
                                                    <div class="form-group">
                                                        <label> Phone </label>
                                                        <input type="tel" class="form-control" value="{{$application->phone}}" name="Phone" placeholder="Relative's Phone Number" >
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Address </label>
                                                        <input type="text" class="form-control" value="{{$application->address}}" name="address" placeholder="Relative's Home Address" >
                                                    </div>
                                                    <div class="form-group">
                                                        <label>Relationship to Child </label>
                                                        <input type="text" class="form-control" value="{{$application->relationshipToChild}}" name="relationshipToChild" placeholder="Relationship to child" >
                                                    </div>

                                                </div>
                                            </div>
                                            {{--</form>--}}




                                        </div>
                                    </div>
                                </div>
                                <!-- /# card -->
                            </div>
                            <!-- /# column -->
                        </div>

                        {{--hear of hendon--}}
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="card alert">
                                    <div class="card-header">
                                        <div class="card-header-right-icon">
                                            <ul>
                                                <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                                <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <div class="menu-upload-form">
                                            <h3> How did You Hear of Hendon  </h3>
                                            <br>

                                            <div class="form-group phone">
                                                {{--<label for="gender">Accomodation</label>--}}
                                                <div class="row checkbox-group" >
                                                    <div class="col-md-4">
                                                        <input id="" type="checkbox" value="Word of Mouth" name="hearOfHendon"
                                                               @if($application->hearOfHendon == "Word of Mouth") checked @endif
                                                        >
                                                        <label for="checkbox3">
                                                            Word of Mouth
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input id="" type="checkbox" value="Current School" name="hearOfHendon"
                                                               @if($application->hearOfHendon == "Current School") checked @endif
                                                        >
                                                        <label for="checkbox3">
                                                            Current School
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input id="" type="checkbox" value="Friend" name="hearOfHendon"
                                                               @if($application->hearOfHendon == "Friend") checked @endif
                                                        >
                                                        <label for="checkbox3">
                                                            Friend
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input id="" type="checkbox" value="Website" name="hearOfHendon"
                                                               @if($application->hearOfHendon == "Website") checked @endif
                                                        >
                                                        <label for="checkbox3">
                                                            Website
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input id="" type="checkbox" value="Advertisement" name="hearOfHendon"
                                                               @if($application->hearOfHendon == "Advertisement") checked @endif
                                                        >
                                                        <label for="checkbox3">
                                                            Advertisement
                                                        </label>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <input id="" type="checkbox" value="Others" name="hearOfHendon"
                                                               @if($application->hearOfHendon == "Others") checked @endif
                                                        >
                                                        <label for="checkbox3">
                                                            Others
                                                        </label>
                                                    </div>

                                                </div>
                                            </div><!--//form-group-->



                                            <div class="form-group">
                                                <div class=" col-sm-10">
                                                    <button type="submit" class="btn btn-lg btn-primary pull-right">Save</button>
                                                </div>
                                            </div>

                                            {{--end form--}}
                                        </div>
                                    </div>
                                </div>
                                <!-- /# card -->
                            </div>
                            <!-- /# column -->
                        </div>



                    </form>


                    <!-- /# row -->
                </div>
                <!-- /# main content -->
            </div>
            <!-- /# container-fluid -->
        </div>
        <!-- /# main -->
    </div>

@endsection


