@extends('layouts.admin')
@section('content')


    <div class="content-wrap">
        <div class="main">
            @include('notification')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Student Details</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->



                <div class="row">
                    <div class="col-md-12">
                         <a href="{{url('manage-applications')}}" class="btn btn-secondary padding-overlay pull-left"> Back to List </a>
                        <a href="{{url('make-student/'.$application->apid)}}" class="btn btn-success padding-overlay pull-right"> Admit </a>
                        <a style="margin-right:10px;" href="{{url('application/'.$application->apid . '/edit')}}" class="btn btn-primary padding-overlay pull-right"> Edit </a>
                        <a href="{{url('application/'.$application->apid . '/delete')}}" class="btn btn-danger padding-overlay pull-right"> Delete </a>
                    </div>
                </div>


                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Student Details</h4>
                                    <div class="card-header-right-icon">
                                        <!-- right side of header -->
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="user-profile m-t-15">
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="user-photo m-b-30">
                                                    @if(!empty($application->image))
                                                        <img class="img-responsive" src="{{$application->image}}" alt="Student Image" />
                                                    @else
                                                        <img class="img-responsive" src="{{url('admin/assets/images/default-image.jpg')}}" alt="Student Image" />

                                                    @endif
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                                <div class="user-profile-name dib">{{$application->surName}} {{$application->foreNames}}</div>

                                                <div class="custom-tab user-profile-tab">
                                                    <ul class="nav nav-tabs" role="tablist">
                                                        <li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">About</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane active" id="1">
                                                            <div class="contact-information">
                                                                <div class="phone-content">
                                                                    <span class="contact-title">Preferred Name:</span>
                                                                    <span class="phone-number">{{$application->preferredName}}</span>
                                                                </div>
                                                                <div class="phone-content">
                                                                    <span class="contact-title">Current School:</span>
                                                                    <span class="phone-number">{{$application->currentSchool}}</span>
                                                                </div>
                                                                <div class="website-content">
                                                                    <span class="contact-title">Proposed Class:</span>
                                                                    <span class="contact-website">{{$application->proposedYear}}</span>
                                                                </div>

                                                                <div class="gender-content">
                                                                    <span class="contact-title">Gender:</span>
                                                                    <span class="gender">{{$application->gender}}</span>
                                                                </div>
                                                                <div class="phone-content">
                                                                    <span class="contact-title">Phone</span>
                                                                    <span class="phone-number">{{$application->homePhone}}</span>
                                                                </div>


                                                                <div class="birthday-content">
                                                                    <span class="contact-title">Date of Birth:</span>
                                                                    <span class="birth-date">{{$application->dob}} </span>
                                                                </div>

                                                                <div class="phone-content">
                                                                    <span class="contact-title">Religion:</span>
                                                                    <span class="phone-number">{{$application->religionDenomination}}</span>
                                                                </div>
                                                                <div class="email-content">
                                                                    <span class="contact-title">Nationality:</span>
                                                                    <span class="contact-email">{{$application->nationality}}</span>
                                                                </div>
                                                                <div class="email-content">
                                                                    <span class="contact-title">First Language:</span>
                                                                    <span class="contact-email">{{$application->firstLanguage}}</span>
                                                                </div>

                                                                <div class="address-content">
                                                                    <span class="contact-title">Home Address:</span>
                                                                    <span class="mail-address">{{$application->homeAddress}}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Father</h4>
                                </div>
                                <div class="card-body">
                                    <div class="user-profile m-t-15">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="user-profile-name dib">{{$application->fatherTitle}} {{$application->father}}</div>
                                                <div class="custom-tab user-profile-tab">
                                                    <ul class="nav nav-tabs" role="tablist">
                                                        <li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">About</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane active" id="1">
                                                            <div class="contact-information">
                                                                <div class="phone-content">
                                                                    <span class="contact-title">Profession:</span>
                                                                    <span class="phone-number">{{$application->fatherProfession}}</span>
                                                                </div>
                                                                <div class="website-content">
                                                                    <span class="contact-title">Industry :</span>
                                                                    <span class="contact-website">{{$application->fatherIndustry}}</span>
                                                                </div>

                                                                <div class="gender-content">
                                                                    <span class="contact-title">Employer Name:</span>
                                                                    <span class="gender">{{$application->fatherEmployerName}}</span>
                                                                </div>
                                                                <div class="phone-content">
                                                                    <span class="contact-title">Phone</span>
                                                                    <span class="phone-number">{{$application->fatherPhone}}</span>
                                                                </div>

                                                                <div class="address-content">
                                                                    <span class="contact-title">Email:</span>
                                                                    <span class="mail-address">{{$application->fatherEmail}}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                        <div class="col-md-6 col-sm-1">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Mother</h4>
                                </div>
                                <div class="card-body">
                                    <div class="user-profile m-t-15">
                                        <div class="row">

                                            <div class="col-lg-12">
                                                <div class="user-profile-name dib">{{$application->motherTitle}} {{$application->mother}}</div>
                                                <div class="custom-tab user-profile-tab">
                                                    <ul class="nav nav-tabs" role="tablist">
                                                        <li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">About</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane active" id="1">
                                                            <div class="contact-information">
                                                                <div class="phone-content">
                                                                    <span class="contact-title">Profession:</span>
                                                                    <span class="phone-number">{{$application->motherProfession}}</span>
                                                                </div>
                                                                <div class="website-content">
                                                                    <span class="contact-title">Industry :</span>
                                                                    <span class="contact-website">{{$application->motherIndustry}}</span>
                                                                </div>

                                                                <div class="gender-content">
                                                                    <span class="contact-title">Employer Name:</span>
                                                                    <span class="gender">{{$application->motherEmployerName}}</span>
                                                                </div>
                                                                <div class="phone-content">
                                                                    <span class="contact-title">Phone</span>
                                                                    <span class="phone-number">{{$application->motherPhone}}</span>
                                                                </div>

                                                                <div class="address-content">
                                                                    <span class="contact-title">Email:</span>
                                                                    <span class="mail-address">{{$application->motherEmail}}</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>
                <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection