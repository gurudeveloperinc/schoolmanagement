@extends('dashboard.parent.layouts.app')
@section('content')

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Welcome </h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Home</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-4">
                            <a href="{{url('parent/children')}}">
                                <div class="card">
                                    <div class="stat-widget-eight">
                                        <div class="stat-header">

                                            @if(count($students) == 1)
                                                <div class="header-title pull-left"> Child</div>
                                            @elseif(count($students) >1)
                                                <div class="header-title pull-left"> Children</div>
                                            @else

                                            @endif

                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="stat-content">
                                            <div class="pull-left">
                                                <span class="stat-digit"> {{count($students)}}</span>
                                            </div>
                                            <div class="pull-right">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-primary w-70" role="progressbar"
                                                 aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">70% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-4">
                            <a href="{{url('parent/news')}}">
                                <div class="card">
                                    <div class="stat-widget-eight">
                                        <div class="stat-header">
                                            <div class="header-title pull-left">Recent News</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="stat-content">
                                            <div class="pull-left">
                                                <span class="stat-digit">{{count($news)}}</span>
                                            </div>
                                            <div class="pull-right">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-warning w-70" role="progressbar"
                                                 aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">70% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="col-lg-4">
                            <a href="{{url('parent/complaints')}}">
                                <div class="card">
                                    <div class="stat-widget-eight">
                                        <div class="stat-header">
                                            <div class="header-title pull-left"></div>
                                            <div class="header-title pull-left"> Complaints</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="stat-content">
                                            <div class="pull-left">

                                                <span class="stat-digit"> {{count($complaints)}}</span>
                                            </div>
                                            <div class="pull-right">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-success w-70" role="progressbar"
                                                 aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">70% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                    </div>


                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header pr">
                                    <h4>Your Children</h4>
                                </div>


                                <div class="visible-sm visible-xs card-body">

                                    @if(count($students) > 0)
										<?php $count = 1; ?>
                                        @foreach($students as $student)
{{--                                            @if( $student->cid == "6")--}}

{{--                                                <p>--}}
{{--                                                    Name - {{$student->fname}} {{$student->sname}} <br>--}}
{{--                                                    @if(isset($student->Class))--}}
{{--                                                        Class - {{$student->Class->name}} <br><br>--}}
{{--                                                    @endif--}}

{{--                                                    RESULT NOT RELEASED YET--}}
{{--                                                    <a href="{{url('parent/child/end-of-term-result-ss3/' . $student->sid)}}"--}}
{{--                                                       class="btn btn-success">View End of term Result</a>--}}

{{--                                                <hr>--}}
{{--                                                </p>--}}

{{--                                            @else--}}

                                                <p>
                                                    Name - {{$student->fname}} {{$student->sname}} <br>
                                                    @if(isset($student->Class))
                                                        Class - {{$student->Class->name}} <br><br>
                                                    @endif
{{--                                                    <a href="{{url('midterm-result-pdf/' . $student->sid)}}"--}}
{{--                                                       class="btn btn-success">View Midterm Result</a>--}}

                                            RESULTS NOT RELEASED YET
{{--                                                <a href="{{url('endofterm-result-pdf/' . $student->sid)}}"--}}
{{--                                                       class="btn btn-success">View End of term Result</a>--}}

                                                <hr>
                                                </p>
                                                {{--<p>--}}
                                                    {{--Name - {{$student->fname}} {{$student->sname}} <br>--}}
                                                    {{--@if(isset($student->Class))--}}
                                                        {{--Class - {{$student->Class->name}} <br><br>--}}
                                                    {{--@endif--}}
                                                    {{--<a href="{{url('parent/child/end-of-term-result/' . $student->sid)}}"--}}
                                                       {{--class="btn btn-success">View End of term Result</a>--}}

                                                {{--<hr>--}}
                                                {{--</p>--}}



{{--                                            @endif--}}
                                        @endforeach
                                    @else
                                        <p>You have no registered children</p>
                                    @endif
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive hidden-xs hidden-sm">
                                        <table class="table table-responsive student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                @if(isset($students[0]->Class))
                                                <th>Class</th>
                                                @endif
                                                {{--<th>Last Position</th>--}}
                                                <th></th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($students) > 0)
												<?php $count = 1; ?>
                                                @foreach($students as $student)
{{--                                                    @if( $student->cid == "6")--}}

{{--                                                        <tr>--}}
{{--                                                            <td>--}}
{{--																<?php echo $count;?>--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                {{$student->fname}} {{$student->sname}}--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                @if(isset($student->Class))--}}
{{--                                                                {{$student->Class->name}}--}}
{{--                                                                @endif--}}
{{--                                                            </td>--}}
{{--                                                            <td>--}}
{{--                                                                RESULT NOT RELEASED YET--}}
{{--                                                            </td>--}}
{{--                                                            --}}{{--<td>--}}
{{--                                                            --}}{{--{{$student->Results[count($student->Results) - 1]->position}}--}}
{{--                                                            --}}{{--</td>--}}
{{--                                                            --}}{{--<td>--}}
{{--                                                                --}}{{--<a href="{{url('parent/child/end-of-term-result-ss3/' . $student->sid)}}"--}}
{{--                                                                   --}}{{--class="btn btn-success">View End of term Result</a>--}}
{{--                                                            --}}{{--</td>--}}
{{--                                                        </tr>--}}
{{--														<?php $count ++; ?>--}}

{{--                                                    @else--}}


                                                        <tr>
                                                            <td>
																<?php echo $count;?>
                                                            </td>
                                                            <td>
                                                                {{$student->fname}} {{$student->sname}}
                                                            </td>
                                                            <td>
                                                                @if(isset($student->Class))
                                                                {{$student->Class->name}}
                                                                @endif
                                                            </td>
{{--                                                            <td>--}}
{{--                                                            {{$student->Results[count($student->Results) - 1]->position}}--}}
{{--                                                            </td>--}}
                                                            <td>
{{--                                                                <a--}}
{{--                                                                        href="{{url('midterm-result-pdf/' . $student->sid)}}"--}}
{{--                                                                   class="btn btn-success">View Midterm Result</a>--}}
{{----}}
                                                                RESULT NOT RELEASED YET

{{--                                                                <a href="{{url('midterm-result-pdf/' . $student->sid)}}"--}}
{{--                                                                   class="btn btn-success">View Midterm Result</a>--}}
                                                            </td>
{{--                                                            <td>--}}
{{--                                                                <a href="{{url('endofterm-result-pdf/' . $student->sid)}}"--}}
{{--                                                                   class="btn btn-success">View End of term Result</a>--}}
{{--                                                            </td>--}}
                                                        </tr>
														<?php $count ++; ?>

{{--                                                    @endif--}}
                                                @endforeach
                                            @else

                                                <tr>
                                                    <td colspan="4"
                                                        style="color: silver; text-align: center; margin-top: 30px;">You
                                                        have no registered children
                                                    </td>
                                                </tr>
                                            @endif
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>


                    <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#"
                                                                                                   class="page-refresh">Refresh
                                        Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection
