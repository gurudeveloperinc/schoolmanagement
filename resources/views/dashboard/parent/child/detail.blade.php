

@extends('dashboard.parent.layouts.app')
@section('content')

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li >Children</li>
                                    <li class="active">Details</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <div id="main-content">
                    <!-- /# column -->

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4> STUDENT INFO</h4>
                                </div>
                                <div class="card-body">
                                    <div class="default-tab">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#home1" aria-controls="home" role="tab" data-toggle="tab">Student</a></li>
                                            {{--<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Health Records</a></li>--}}
                                            <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Results</a></li>
                                            <li role="presentation"><a href="#assignments" aria-controls="assignments" role="tab" data-toggle="tab">Assignments</a></li>
                                            {{--<li role="presentation"><a href="#subjects" aria-controls="subjects" role="tab" data-toggle="tab">Subjects</a></li>--}}
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="home1">
                                                {{--<h3>*Note</h3>--}}

                                                {{--first card--}}
                                                <div class="card-body">
                                                    <div class="user-profile m-t-15">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="user-photo m-b-30">

                                                                    @if(!empty($student->image))
                                                                        <img class="img-responsive"
                                                                             src="{{$student->image}}"
                                                                             alt="Student Image"/>
                                                                    @else
                                                                        <img class="img-responsive"
                                                                             src="{{url('admin/assets/images/default-image.jpg')}}"
                                                                             alt="Student Image"/>

                                                                    @endif

                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="user-profile-name dib">{{$student->fname}} {{$student->sname}}</div>
                                                                <div class="custom-tab user-profile-tab">
                                                                    <ul class="nav nav-tabs" role="tablist">
                                                                        <li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">About</a></li>
                                                                    </ul>
                                                                    <div class="tab-content">
                                                                        <div role="tabpanel" class="tab-pane active" id="1">
                                                                            <div class="contact-information">
                                                                                <div class="phone-content">
                                                                                    <span class="contact-title">Student ID:</span>
                                                                                    {{--                                                                                    <span class="phone-number">{{$student->preferredName}}</span>--}}
                                                                                </div>
                                                                                @if(isset($student->Focus))
                                                                                <div class="phone-content">
                                                                                    <span class="contact-title"> Focus:</span>
                                                                                    <span class="phone-number">{{$student->Focus->name}}</span>
                                                                                </div>
                                                                                @endif
                                                                                <div class="website-content">
                                                                                    <span class="contact-title">Class:</span>
                                                                                    <span class="contact-website">{{$student->class->name}}</span>
                                                                                </div>

                                                                                <div class="gender-content">
                                                                                    <span class="contact-title">Gender:</span>
                                                                                    <span class="gender">{{$student->gender}}</span>
                                                                                </div>
                                                                                <div class="phone-content">
                                                                                    <span class="contact-title">Phone</span>
                                                                                    <span class="phone-number">{{$student->phone}}</span>
                                                                                </div>


                                                                                <div class="birthday-content">
                                                                                    <span class="contact-title">Date of Birth:</span>
                                                                                    <span class="birth-date">{{$student->dob}} </span>
                                                                                </div>

                                                                                <div class="phone-content">
                                                                                    <span class="contact-title">Religion:</span>
                                                                                    {{--                                                                                    <span class="phone-number">{{$application->religionDenomination}}</span>--}}
                                                                                </div>
                                                                                <div class="email-content">
                                                                                    <span class="contact-title">Nationality:</span>
                                                                                    <span class="contact-email">{{$student->nationality}}</span>
                                                                                </div>
                                                                                <div class="email-content">
                                                                                    <span class="contact-title">First Language:</span>
                                                                                    {{--                                                                                    <span class="contact-email">{{$application->firstLanguage}}</span>--}}
                                                                                </div>

                                                                                <div class="address-content">
                                                                                    <span class="contact-title">Home Address:</span>
                                                                                    <span class="mail-address">{{$student->address}}</span>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{--end of first card--}}
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="profile">


												<?php $count = 1; ?>

                                                @if(count($health_record)>0)
                                                @foreach($health_record as $health)
                                                    <div class="row">
                                                        <div class="col-lg-10 col-md-10">
                                                            <div class="card alert">
                                                                <div class="card-header">
                                                                    <h4>Diagnosis</h4>
                                                                </div>
                                                                <div class="card-body">
                                                                    <p class="text-muted m-b-15">
                                                                        {{str_limit($health->diagnosis,100,' ....')}}
                                                                    </p>
                                                                    <button class="btn btn-danger m-b-10 pull-right" data-toggle="collapse" data-target="#basic-collapse{{$count}}">More Details</button>

                                                                    <div id="basic-collapse{{$count}}" class="collapse">
                                                                        <h4> Detail </h4>
                                                                        {{$health->details}}
                                                                        <br>
                                                                        <br>
                                                                        <h4> Treatment </h4>
                                                                        {{$health->treatment}}
                                                                    </div>

                                                                </div>
                                                            </div>
                                                            <!-- /# card -->
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <hr>
													<?php $count++; ?>

                                                @endforeach
                                                    @else
                                                    <h3 style="text-align: center;color: silver; margin-top: 35px;"> There are no Records for this Child</h3>
                                                    @endif


                                                {{--health record--}}






                                                {{--<p> This table Holds the Health status of the student and reports of any treatment administered at the school </p>--}}

                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="messages">
                                                RESULT NOT RELEASED YET
                                                {{--<a href="{{url('midterm-result-pdf/' . $student->sid)}}" class="btn btn-success">View Midterm Result</a>--}}
{{--                                                <a href="{{url('parent/child/end-of-term-result/' . $student->sid)}}" class="btn btn-success">View End of term Result</a>--}}
                                                {{--<p>This holds a report of the academic performance of the student,complaints and any suggestion for academics</p>--}}
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="assignments">
                                                <table class="table table-hover table-striped">
                                                    <tr>
                                                        <th>S/N</th>
                                                        <th>Name</th>
                                                        <th>Subject</th>
                                                        <th>File</th>
                                                        <th>Date Given</th>
                                                        <th>Due Date</th>
                                                        <th>Status</th>
                                                        <th>Score</th>
                                                    </tr>

                                                    <?php $count = 1; ?>
                                                    @foreach($assignments as $assignment)
                                                    <tr>

                                                        <td>{{$count}}</td>
                                                        <td>{{$assignment->name}}</td>
                                                        <td>{{$assignment->Subject->name}}</td>
                                                        <td>
                                                            <a class="btn btn-success" href="{{$assignment->url}}">Download</a>
                                                        </td>
                                                        <td>{{$assignment->created_at->toDayDateTimeString()}}
                                                            <small>({{$assignment->created_at->diffForHumans()}})</small>
                                                        </td>
                                                        <td>{{$assignment->due->toDayDateTimeString()}}
                                                            <small>({{$assignment->due->diffForHumans()}})</small>
                                                        </td>
                                                        <td>
                                                            @if(count($assignment->Submission) > 0)
                                                            <span class="label label-success">SUBMITTED</span><br>
                                                                <small> Submitted on {{$assignment->Submission->created_at->toDayDateTimeString()}}</small>
                                                            @else
                                                                <span class="label label-danger">NOT SUBMITTED</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(count($assignment->Submission) > 0)
                                                                @if(isset($assignment->Submission->grade))
                                                                    {{$assignment->Submission->grade}} / {{$assignment->total}}
                                                                @else
                                                                    Not Graded
                                                                @endif
                                                            @else
                                                                Not Graded
                                                            @endif

                                                        </td>
                                                    </tr>
                                                        <?php $count++; ?>
                                                    @endforeach
                                                </table>
                                            </div>

                                            <div role="tabpanel" class="tab-pane" id="subjects">

                                                <h3>Subjects offered by your child</h3>
                                                <table class="table ">

                                                    <tr>
                                                        <th>S/N</th>
                                                        <th>Name</th>
                                                    </tr>
		                                            <?php $count = 1; ?>

                                                    @if(count($student->Subjects) <= 0)
                                                        <tr>
                                                            <td colspan="2" style="text-align: center">No subjects yet</td>
                                                        </tr>
                                                    @endif

                                                    @foreach($student->Subjects as $subject)
                                                        <tr>
                                                            <td>{{$count}}</td>
                                                            <td>{{$subject->name}}</td>
                                                        </tr>
			                                            <?php $count++; ?>
                                                    @endforeach
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                        <!-- /# column -->
                    </div>



                <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>







    {{--@foreach($health_record as $health)--}}
        {{--<div class="card-body">--}}
            {{--<div class="user-profile m-t-15">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-lg-2">--}}
                        {{--<div class="user-photo m-b-30">--}}
                            {{--<img class="img-responsive" src="{{$student->image}}" alt="Photo Space" />--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-lg-10">--}}
                        {{--<div class="user-profile-name dib">{{$health->diagnosis}} </div>--}}
                        {{--<div class="useful-icon dib pull-right">--}}
                            {{--<span><a href="{{url('/student/'.$student->sid.'/edit')}}" class="btn btn-danger"><i class="ti-pencil-alt"></i></a> </span>--}}
                            {{--<span><a href="#"><i class="ti-printer"></i></a></span>--}}
                            {{--<span><a href="#"><i class="ti-download"></i></a></span>--}}
                            {{--<span><a href="#"><i class="ti-share"></i></a></span>--}}
                        {{--</div>--}}
                        {{--<div class="custom-tab user-profile-tab">--}}
                            {{--<ul class="nav nav-tabs" role="tablist">--}}
                                {{--<li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">Treatment Details</a></li>--}}
                            {{--</ul>--}}
                            {{--<div class="tab-content">--}}
                            {{--<div role="tabpanel" class="tab-pane active" id="1">--}}
                            {{--<div class="contact-information">--}}
                            {{--<div class="phone-content">--}}
                            {{--<h3><span  class="contact-title">Details:</span></h3>--}}
                            {{--<p>{{$health->details}}</p>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}

                            {{--<div class="tab-content">--}}
                                {{--<div role="tabpanel" class="tab-pane active" id="1">--}}
                                    {{--<div class="contact-information">--}}
                                        {{--<div class="phone-content">--}}
                                            {{--<h3><span class="contact-title">Treatment:</span></h3>--}}
                                            {{--<p>{{str_limit($health->treatment,10 ,'  ....')}}</p>--}}

                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<br>--}}
        {{--<br>--}}

        {{--<hr>--}}
        {{--<br>--}}
    {{--@endforeach--}}

@endsection









