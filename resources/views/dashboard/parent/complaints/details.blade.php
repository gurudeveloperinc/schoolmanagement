

@extends('dashboard.parent.layouts.app')
@section('content')

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Complaints</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <div id="main-content">
                    <!-- /# column -->

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4> COMPLAINT ID - {{$complaint->comid}}</h4>
                                </div>
                                <div class="card-body">
                                    <div class="default-tab">
                                        {{--<ul class="nav nav-tabs" role="tablist">--}}
                                            {{--<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Conversation</a></li>--}}
                                            {{--<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"> Finance</a></li>--}}
                                        {{--</ul>--}}
                                        <div class="tab-content">

                                            <div role="tabpanel" class="tab-pane active" id="messages">
                                                <div class="card">

                                                    {{$complaint->complaint}}

                                                    <br>
                                                    {{--<div class="recent-comment m-t-15">--}}
                                                        {{--<div class="media">--}}
                                                            {{--<div class="media-left">--}}
                                                                {{--<a href="#"><img class="media-object" src="assets/images/avatar/1.jpg" alt="..."></a>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="media-body">--}}
                                                                {{--<h4 class="media-heading color-primary">Mr.  Ajay</h4>--}}
                                                                {{--<p>Cras sit amet nibh libero, in gravida nulla.</p>--}}
                                                                {{--<p class="comment-date">10 min ago</p>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="media">--}}
                                                            {{--<div class="media-left">--}}
                                                                {{--<a href="#"><img class="media-object" src="assets/images/avatar/2.jpg" alt="..."></a>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="media-body">--}}
                                                                {{--<h4 class="media-heading color-success">Mr.  Ajay</h4>--}}
                                                                {{--<p>Cras sit amet nibh libero, in gravida nulla.</p>--}}
                                                                {{--<p class="comment-date">1 hour ago</p>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="media">--}}
                                                            {{--<div class="media-left">--}}
                                                                {{--<a href="#"><img class="media-object" src="assets/images/avatar/3.jpg" alt="..."></a>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="media-body">--}}
                                                                {{--<h4 class="media-heading color-danger">Mr.  Ajay</h4>--}}
                                                                {{--<p>Cras sit amet nibh libero, in gravida nulla.</p>--}}
                                                                {{--<div class="comment-date">Yesterday</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                        {{--<div class="media no-border">--}}
                                                            {{--<div class="media-left">--}}
                                                                {{--<a href="#"><img class="media-object" src="assets/images/avatar/3.jpg" alt="..."></a>--}}
                                                            {{--</div>--}}
                                                            {{--<div class="media-body">--}}
                                                                {{--<h4 class="media-heading color-info">Mr.  Ajay</h4>--}}
                                                                {{--<p>Cras sit amet nibh libero, in gravida nulla.</p>--}}
                                                                {{--<div class="comment-date">Yesterday</div>--}}
                                                            {{--</div>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                </div>

                                                @if(isset($complaint->Student))
                                                    <h4>Related To</h4>
                                                <p>
                                                    Name - {{$complaint->Student->fname}} {{$complaint->Student->sname}} <br>
                                                    @if( isset($complaint->Student->Class))
                                                    Class - {{$complaint->Student->Class->name}} <br>
                                                    @endif
                                                </p>
                                                @endif
                                                <p>
                                                    Date - {{$complaint->created_at->toDayDateTimeString()}}
                                                </p>

                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="settings">
                                                <p>This tab holds the financial record of the students payments and pending payments.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                        <!-- /# column -->
                    </div>


                <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection









