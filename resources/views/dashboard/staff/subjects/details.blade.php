<?php use Carbon\Carbon; ?>
@extends('dashboard.staff.layouts.app')
@section('content')


    <div class="content-wrap">
        <div class="main">
            @include('notification')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Subject Detail</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->



                <div class="row">
                    <div class="col-md-8 col-md-offset-1">


                        <div class="col-md-3 pull-left">
                            {{--                            <a href="{{url('student/'.$student->sid.'/edit')}}" class="btn btn-info padding-overlay"> Update record </a>--}}
                        </div>

                        <div class="col-md-3">
                            {{--<a href="{{url('admit-student/'.$application->apid)}}" class="btn btn-success padding-overlay"> Admit Student</a>--}}
                        </div>

                        <div class="col-md-3">
                            {{--                            <a href="{{url('make-student/'.$application->apid)}}" class="btn btn-danger padding-overlay pull-right"> Admit </a>--}}
                        </div>

                    </div>

                </div>
                <!-- /# row -->



                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Subject Details - {{$subject->name}}</h4>

                                </div>
                                <div class="card-body">
                                    <div class="user-profile m-t-15">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="user-profile-name dib">Teachers </div>
                                                <div class="useful-icon dib pull-right">
                                                    <span><a href="{{url('subject/'.$subject->subid.'/edit')}}" class="btn btn-danger"><i class="ti-pencil-alt"></i></a> </span>
                                                </div>


                                                            <div class="contact-information col-12">
                                                                <div class="col-md-6">

                                                                    <div class="phone-content">
                                                                        <span class="contact-title"> Name:</span>
                                                                        <span class="phone-number">   {{$subject->Teacher->fname}}  {{$subject->Teacher->sname}}

			                                                                <?php

			                                                                if(!empty($subject->cids)){
				                                                                $cids = json_decode($subject->cids);

				                                                                $assignedClasses = "(";
				                                                                $countCids = count($cids);

				                                                                $count = 1;
				                                                                foreach ($cids as $cid){
					                                                                $assignedClasses .= \App\classes::find($cid)->name ;

					                                                                if($countCids != $count) $assignedClasses .= ",";

					                                                                $count++;
				                                                                }

				                                                                $assignedClasses .= ")";

				                                                                echo $assignedClasses;
			                                                                }

			                                                                ?>
                                                                    </span>

                                                                    </div>

                                                                    <div class="phone-content">
                                                                        <span class="contact-title"> Phone:</span>
                                                                        <span class="phone-number">{{$subject->Teacher->phone}} </span>
                                                                    </div>
                                                                    <div class="website-content">
                                                                        <span class="contact-title">Email:</span>
                                                                        <span class="contact-website">{{$subject->Teacher->email}}</span>
                                                                    </div>

                                                                </div>


                                                                <!-- show extra subject teachers -->
                                                                @foreach($subject->Teachers as $teacher)
                                                                    <div class="col-md-6">

                                                                        <div class="phone-content">
                                                                            <span class="contact-title"> Name:</span>
                                                                            <span class="phone-number">   {{$teacher->fname}}  {{$teacher->sname}}

			                                                                    <?php


			                                                                    $st = \App\subjectTeacher::where('stid',$teacher->stid)->where('subid',$subject->subid)->first();
			                                                                    if(!empty($st->cids)){
				                                                                    $cids = json_decode($st->cids);

				                                                                    $assignedClasses = "(";
				                                                                    $countCids = count($cids);

				                                                                    $count = 1;
				                                                                    foreach ($cids as $cid){
					                                                                    $assignedClasses .= \App\classes::find($cid)->name ;

					                                                                    if($countCids != $count) $assignedClasses .= ",";

					                                                                    $count++;
				                                                                    }

				                                                                    $assignedClasses .= ")";

				                                                                    echo $assignedClasses;
			                                                                    }

			                                                                    ?>
                                                                        </span>
                                                                        </div>

                                                                        <div class="phone-content">
                                                                            <span class="contact-title"> Phone:</span>
                                                                            <span class="phone-number">{{$subject->Teacher->phone}} </span>
                                                                        </div>
                                                                        <div class="website-content">
                                                                            <span class="contact-title">Email:</span>
                                                                            <span class="contact-website">{{$subject->Teacher->email}}</span>
                                                                        </div>



                                                                    </div>
                                                                @endforeach

                                                            </div>


                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>


                    <div class="card">

                        <div class="default-tab">

                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#updates" aria-controls="updates"
                                                                          role="tab" data-toggle="tab">Updates</a>
                                </li>
                                <li role="presentation">
                                    <a href="#tests" aria-controls="tests" role="tab" data-toggle="tab">Tests</a>
                                </li>
                                <li role="presentation">
                                    <a href="#assignments" aria-controls="assignments" role="tab" data-toggle="tab">Assignments</a>
                                </li>
                                <li role="presentation">
                                    <a href="#files" aria-controls="files" role="tab" data-toggle="tab"> Files</a>
                                </li>
                                <li role="presentation">
                                    <a href="#students" aria-controls="students" role="tab" data-toggle="tab">Students</a>
                                </li>
                            </ul>

                            <!-- Tabs -->
                            <div class="tab-content">

                                <!-- Update Tab -->
                                <div role="tabpanel" class="tab-pane active" id="updates">

                                    <div>
                                        <form method="post" action="{{url('staff/subject/update/add')}}">
                                            @csrf
                                            <input type="hidden" name="subid" value="{{$subject->subid}}">
                                            <input type="hidden" name="stid" value="{{session()->get('staff')->stid}}">
                                            <label>Post an update</label>
                                            <textarea class="form-control" name="update" rows="5" placeholder="Type your update here then click post."></textarea><br>
                                            <button type="submit" class="btn btn-success">Post</button>
                                        </form>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="card alert">
                                            <div class="card-header">
                                                <h4>Recent Updates </h4>
                                            </div>

                                            <div class="recent-comment m-t-20">

                                                @foreach($subject->Updates->reverse() as $update)


                                                    <div class="media">
                                                        <div class="media-left">
                                                            @if(!empty($update->Teacher->image))
                                                                <a href="#">
                                                                    <img class="media-object" style="width: 100px" src="{{$update->Teacher->image}}" alt="Teacher's Image">
                                                                </a>
                                                            @else
                                                                <a href="#">
                                                                    <img class="media-object" style="width: 100px" src="{{url('admin/assets/images/default-image.jpg')}}" alt="Teacher's Image">
                                                                </a>
                                                            @endif
                                                        </div>
                                                        <div class="media-body">
                                                            <h4 class="media-heading color-deafult">{{$update->created_at->toDayDateTimeString()}}</h4>
                                                            <h4 class="media-heading color-primary">{{$update->Teacher->fname}} {{$update->Teacher->sname}}</h4>
                                                            <p> {{$update->update}}</p>
                                                            <p class="comment-date">{{$update->created_at->diffForHumans()}}<br>
                                                            <a href="{{url('staff/subject/update/'.$update->suid. '/delete')}}" style="color:white;" class="badge badge-danger">Delete</a></p>
                                                        </div>
                                                    </div>

                                                @endforeach

                                            </div>
                                        </div>
                                        <!-- /# card -->
                                    </div>


                                </div>

                                <!-- Tests Tab -->
                                <div role="tabpanel" class="tab-pane " id="tests">
                                    <table class="table student-data-table m-t-20">
                                        <thead>
                                        <tr>
                                            <th>S/N</th>
                                            <th>Name</th>
                                            <th>Available Between</th>
                                            <th>Classes</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if(count($tests)>0)
						                    <?php $count = 1; ?>
                                            @foreach($tests as $test)
                                                <tr>

                                                    <td>
                                                        # <?php echo $count; ?>
                                                    </td>
                                                    <td>
                                                        {{$test->name}}
                                                    </td>
                                                    <td>
                                                        {{Carbon::createFromFormat("Y-m-d H:i:s",$test->startTime)->toDayDateTimeString()}} and
                                                        {{Carbon::createFromFormat("Y-m-d H:i:s",$test->endTime)->toDayDateTimeString()}}
                                                    </td>

                                                    <td>
                                                        @foreach($test->Classes as $item)
                                                            {{$item->name}},
                                                        @endforeach
                                                    </td>
                                                    <td>
                                                        {{--@if($test->endTime < Carbon::now())--}}
                                                            {{--<span>--}}
                                                                <a class="btn btn-success" href="{{url('staff/test-results/' . $test->testid)}}">
                                                                    {{--<i class="ti-eye color-default"></i>--}}
                                                                    View Results
                                                                </a>
                                                            {{--</span>--}}
                                                        {{--@endif--}}
                                                    </td>
                                                </tr>
							                    <?php $count ++; ?>
                                            @endforeach
                                        @else

                                            <tr>
                                                <td colspan="5" style="color: silver; text-align: center; margin-top: 30px;"> There are no tests </td>
                                            </tr>

                                        @endif




                                        </tbody>
                                    </table>
                                </div>

                                <!-- Assignments Tab -->
                                <div role="tabpanel" class="tab-pane" id="assignments">

                                    <table class="table student-data-table m-t-20">
                                        <thead>
                                        <tr>
                                            <th>S/N</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Class</th>
                                            <th>Due</th>
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if(count($assignments)>0)
						                    <?php $count = 1; ?>
                                            @foreach($assignments as $assignment)
                                                <tr>

                                                    <td>
                                                        <?php echo $count; ?>
                                                    </td>
                                                    <td>
                                                        {{$assignment->name}}
                                                    </td>
                                                    <td>
                                                        {{str_limit($assignment->description,300,'...')}}
                                                    </td>
                                                    <td>
                                                        {{$assignment->Class->name}}
                                                    </td>
                                                    <td>
                                                        {{$assignment->due->toDayDateTimeString()}}
                                                    </td>

                                                    <td>
                                                        <span><a href="{{url('staff/assignment/'. $assignment->aid )}}">View</a> </span>
                                                    </td>
                                                </tr>
							                    <?php $count ++; ?>
                                            @endforeach
                                        @else

                                            <tr>
                                                <td colspan="5" style="color: silver; text-align: center; margin-top: 30px;"> There are no tests </td>
                                            </tr>

                                        @endif




                                        </tbody>
                                    </table>

                                </div>

                                <!-- Files Tab -->
                                <div role="tabpanel" class="tab-pane" id="files">
                                    <form method="post" enctype="multipart/form-data" action="{{url('staff/subject/file/add')}}">
                                        @csrf
                                        <input type="hidden" name="subid" value="{{$subject->subid}}">
                                        <input type="hidden" name="stid" value="{{session()->get('staff')->stid}}">

                                        <div class="form-group">
                                            <label>Description</label>
                                            <textarea class="form-control" maxlength="1000" placeholder="Add a description. This is optional." name="description"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Add File(s)</label>
                                            <input type="file" multiple name="files[]">
                                        </div>
                                        <button class="btn btn-success">Upload</button>

                                    </form>

                                    <br><br><hr>

                                    @foreach($subject->Files as $file)
                                    <div style="height:300px;overflow:scroll;border:1px solid grey;margin-top:10px;" class="col-md-4">

                                        <b>File #{{$file->sfid}}</b>
                                        <br>
                                            {{$file->description}}
                                        <br>
                                        <p>
                                            <a href="{{$file->url}}" class="badge badge-success">Download</a>
                                            <a href="{{url('staff/subject/file/' . $file->sfid . '/delete')}}" class="badge badge-danger">Delete</a>
                                        </p>
                                    </div>
                                    @endforeach


                                </div> <!-- end files tab -->

                                <!-- Students -->
                                <div role="tabpanel" class="tab-pane" id="students">

                                    <table class="table table-hover student-data-table m-t-20">
                                        <thead>
                                        <tr>
                                            <th>S/N</th>
                                            <th>Name</th>
                                            <th>Gender</th>
                                            <th>Class</th>
                                            {{--<th>Created</th>--}}
                                            <th></th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                        @if(count($subject->Students)>0)

			                                <?php $count = 1; ?>

                                            @foreach($subject->Students as $student)
                                                <tr>
                                                    <td>
                                                        <?php echo $count;?>
                                                    </td>
                                                    <td>
                                                        {{strtoupper($student->fname)}} {{strtoupper($student->sname)}}
                                                    </td>
                                                    <td>
                                                        {{$student->gender}}
                                                    </td>
                                                    <td>
                                                        @if(isset($student->Class))
                                                            {{$student->Class->name}}
                                                        @else
                                                            No Class Assigned
                                                        @endif
                                                    </td>
                                                    {{--<td>--}}
{{--                                                        {{$student->created_at->toDayDateTimeString()}}--}}
                                                    {{--</td>--}}


                                                    {{--<td class="actions">--}}
                                                        {{--<span><a href="{{url('student/'.$student->sid.'/detail')}}"><i class="ti-eye color-default"></i></a> </span>--}}
                                                    {{--</td>--}}
                                                </tr>
				                                <?php $count ++; ?>
                                            @endforeach
                                        @else

                                            <tr>
                                                <td style="color: silver; text-align: center; margin-top: 30px;"> There are no Students yet </td>
                                            </tr>

                                        @endif




                                        </tbody>
                                    </table>


                                </div> <!-- end students tab -->

                            </div>
                        </div>
                    </div>


                    <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
