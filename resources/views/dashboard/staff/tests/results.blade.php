<?php use Carbon\Carbon; ?>
@extends('dashboard.staff.layouts.app')
@section('content')
    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">

                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li>Tests</li>
                                    <li class="active">Results</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>

                @include('notification')

                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header pr">
                                    <h4>Test Results for {{$test->name}} </h4>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Student Name</th>
                                                <th>Student Class</th>
                                                <th>Score</th>
                                                <th>Attempt</th>
                                                {{--<th></th>--}}
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($test->Results)>0)
												<?php $count = 1; ?>
                                                @foreach($test->Results as $result)
                                                    <tr>

                                                        <td>
                                                            # <?php echo $count; ?>
                                                        </td>
                                                        <td>
                                                            {{$result->Student->fname}} {{$result->Student->sname}}
                                                        </td>
                                                        <td>
                                                            {{$result->Student->Class->name}}
                                                        </td>
                                                        <td>
                                                            {{$result->score}} / {{$result->total}}
                                                        </td>
                                                        <td>
                                                            {{$result->attempt}}
                                                        </td>
                                                        {{--<td>--}}
                                                            {{--<span><a href="{{url('test/' . $test->testid)}}"><i class="ti-eye color-default"></i></a> </span>--}}
                                                        {{--</td>--}}
                                                    </tr>
													<?php $count ++; ?>
                                                @endforeach
                                            @else

                                                <tr>
                                                    <td colspan="5" style="color: silver; text-align: center; margin-top: 30px;"> There are no attempts yet </td>
                                                </tr>
                                            @endif




                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /# main content -->
            </div>
            <!-- /# container-fluid -->
        </div>
        <!-- /# main -->
    </div>


@endsection







