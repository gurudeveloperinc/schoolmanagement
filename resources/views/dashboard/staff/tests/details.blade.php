<?php use Carbon\Carbon; ?>
@extends('dashboard.staff.layouts.app')
@section('content')




    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Tests</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-body">

                                     <a href="{{url('staff/test-results/' . $test->testid)}}" class="btn btn-success pull-right" style="margin-left: 10px;">View Results</a>
                                     <a href="{{url('staff/test/' . $test->testid . '/questions/delete')}}" class="btn btn-danger pull-right" style="margin-left: 10px;">Delete All Questions</a>

                                    @if(empty($test->deadline) || ( !empty($test->deadline) && Carbon::now() < $test->deadline ) )
                                    <a href="{{url('staff/upload-questions/' . $test->testid)}}" class="btn btn-primary pull-right">Upload Questions</a>
                                    <a href="{{url('staff/add-question/' . $test->testid)}}" class="btn btn-primary pull-right" style="margin-right: 10px;">Add Question</a>
                                    @endif


                                    <p class="page-title">Test - {{$test->name}}</p>
                                    <p>
                                        {{count($test->Questions)}} question(s) <br>
                                        Duration - {{$test->duration}} mins <br>

                                        Description - {{$test->description}} <br>
                                        Subject - <a href="{{url('staff/subject/' . $test->Subject->subid)}}">
                                            {{$test->Subject->name}}
                                        </a>  <br>
                                        Available between <b>{{Carbon::createFromFormat("Y-m-d H:i:s",$test->startTime)->toDayDateTimeString()}}</b>
                                        and <b>{{Carbon::createFromFormat("Y-m-d H:i:s",$test->endTime)->toDayDateTimeString()}}</b> <br>
                                        Attempts - {{$test->attempts}}

                                        @if(isset($test->deadline))
                                            Submission Deadline -
                                            @if(isset($test->deadline) && Carbon::now()->diffInDays($test->deadline) < 7)
                                                <span class="text-danger">{{$test->deadline->toDayDateTimeString()}}</span> ({{$test->deadline->diffForHumans()}})
                                            @endif
                                            @if(isset($test->deadline) && Carbon::now()->diffInDays($test->deadline) > 7 && Carbon::now()->diffInDays($test->deadline) < 14)
                                                <span class="text-warning">{{$test->deadline->toDayDateTimeString()}}</span> ({{$test->deadline->diffForHumans()}})
                                            @endif
                                            @if(isset($test->deadline) && Carbon::now()->diffInDays($test->deadline) > 14)
                                                <span class="text-success">{{$test->deadline->toDayDateTimeString()}} </span> ({{$test->deadline->diffForHumans()}})
                                            @endif

                                        @endif
                                    </p>
                                </div>
                            </div>


                            <div class="card alert">
                                <div class="card-body" align="center">

                                    <form class="form-inline" method="post" action="{{url('staff/edit-test')}}">
                                        @csrf
                                        <input type="hidden" name="testid" value="{{$test->testid}}" >

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Number of attempts</label>
                                            <div class="col-sm-9">
                                                <input  type="text" class="form-control" name="attempts" required placeholder="">
                                            </div>
                                        </div>


                                        <button class="btn btn-success">Change</button>
                                    </form>

                                </div>
                            </div>


                            <div class="card alert">
                                <div class="card-body" align="center">

                                    <form class="form-inline" method="post" action="{{url('staff/edit-test')}}">
                                        @csrf
                                        <input type="hidden" name="testid" value="{{$test->testid}}" >

                                        <p><b>Available between</b></p>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">Start Time</label>
                                            <div class="col-sm-9">
                                                <input type="text" id="startTime" class="form-control" name="startTime" required placeholder="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-3 control-label">End Time</label>
                                            <div class="col-sm-9">
                                                <input id="endTime" type="text" class="form-control" name="endTime" required placeholder="">
                                            </div>
                                        </div>


                                        <button class="btn btn-success">Change</button>
                                    </form>

                                </div>
                            </div>


                            <div class="card alert">
                                <div class="card-body">

                                    <h6>Questions</h6>

                                    <div class="table-responsive">
                                        <table class="table student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/n</th>
                                                <th>Question</th>
                                                <th>Created</th>
                                                <th></th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if( count($test->Questions) > 0 )

												<?php $count = 1; ?>

                                                @foreach($test->Questions as $question)
                                                    <tr>

                                                        <td>
                                                            #<?php echo $count;?>
                                                        </td>
                                                        <td>
                                                            {{$question->question}}
                                                        </td>
                                                        <td>
                                                            {{$question->created_at->diffForHumans()}}
                                                        </td>

                                                        <td>
                                                            <span><a href="{{url('staff/question/' . $question->qid)}}"><i class="ti-eye color-default"></i></a> </span>
                                                            <span><a href="{{url('staff/question/' . $question->qid . '/delete')}}"><i class="ti-trash color-danger"></i> </a></span>
                                                        </td>
                                                    </tr>
													<?php $count ++; ?>
                                                @endforeach
                                            @else
                                                <tr>
                                                    <td colspan="4" style="text-align: center">There are no questions for this test</td>
                                                </tr>
                                            @endif




                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script>
        $(document).ready( function() {

            $('#startTime').datetimepicker();
            $('#endTime').datetimepicker();

        } );
    </script>




@endsection