<?php use Carbon\Carbon; ?>
@extends('dashboard.staff.layouts.app')
@section('content')


    <div class="content-wrap">
        <div class="main">
            @include('notification')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Class Details</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->



                <div class="row">
                    <div class="col-md-8 col-md-offset-1">


                        <div class="col-md-3 pull-left">
                            {{--                            <a href="{{url('student/'.$student->sid.'/edit')}}" class="btn btn-info padding-overlay"> Update record </a>--}}
                        </div>

                        <div class="col-md-3">
                            {{--<a href="{{url('admit-student/'.$application->apid)}}" class="btn btn-success padding-overlay"> Admit Student</a>--}}
                        </div>

                        <div class="col-md-3">
                            {{--                            <a href="{{url('make-student/'.$application->apid)}}" class="btn btn-danger padding-overlay pull-right"> Admit </a>--}}
                        </div>

                    </div>

                </div>
                <!-- /# row -->



                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Class Details</h4>

                                    <a style="float:right;margin-left:10px;" href="{{url('class-result-summary/' . $class->cid)}}" class="btn btn-success">Print Summary</a>
                                    @if($class->cid == "6" || $class->cid == "3")
                                    <a style="float:right;margin-left:10px;" href="{{url('class-result-summary-mock/' . $class->cid)}}" class="btn btn-success">Print Mock Summary</a>
                                    @endif

                                    <a style="float:right;" href="{{url('staff/broad-sheet/' . $class->cid)}}" class="btn btn-success">Generate Broadsheet</a>
                                    <a style="float:right;margin-right:10px;" href="{{url('staff/midterm-broad-sheet/' . $class->cid)}}" class="btn btn-success">Generate Midterm Broadsheet</a>
                                    @if($class->cid == "6" || $class->cid == "3")
                                        <a style="margin-right:10px;float:right;" href="{{url('staff/broad-sheet-ss3/' . $class->cid)}}" class="btn btn-success">Generate Mock Broadsheet</a>
                                    @endif
                                </div>
                                <div class="card-body">
                                    <div class="user-profile m-t-15">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                @if($class->cid == "6" || $class->cid == "3")
                                                <a href="{{url('verify-endofterm-result-ss3?cid=' . $class->cid)}}" class="btn btn-outline-success pull-right">Verify End of term Mock Result</a>
                                                <a href="{{url('setup-endofterm-result-ss3/'  . $class->cid)}}" class="btn btn-outline-success pull-right">Generate End of term Mock Result</a>
                                                @endif

                                                <a href="{{url('verify-endofterm-result?cid='  . $class->cid)}}" class="btn btn-outline-success pull-right">Verify End of term Result</a>
                                                <a href="{{url('setup-endofterm-result/' . $class->cid)}}" class="btn btn-outline-success pull-right">Generate End of term Result</a>

                                                <a href="{{url('verify-midterm-result?cid=' . $class->cid)}}" class="btn btn-outline-success pull-right">Verify Mid-term Result</a>
                                                <a href="{{url('setup-midterm-result/' . $class->cid)}}" class="btn btn-outline-success pull-right">Generate Mid-term Result</a>


                                                <a href="{{url('verify-annual-result?cid='  . $class->cid)}}" class="btn btn-outline-success pull-right">Verify Annual Result</a>
                                                <a href="{{url('setup-annual-result/' . $class->cid)}}" class="btn btn-outline-success pull-right">Generate Annual Result</a>

                                                    <div class="user-profile-name dib">{{$class->name}} </div>
                                                <div class="custom-tab user-profile-tab">
                                                    <ul class="nav nav-tabs" role="tablist">
                                                        <li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">About</a></li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <div role="tabpanel" class="tab-pane active" id="1">
                                                            <div class="contact-information">
                                                                @if(isset($class->Teacher))
                                                                    <div class="phone-content">
                                                                        <span class="contact-title"> Class Teacher:</span>
                                                                            <span class="phone-number">   {{$class->Teacher->fname}}  {{$class->Teacher->sname}} </span>
                                                                    </div>
                                                                    <div class="phone-content">
                                                                        <span class="contact-title"> Phone:</span>
                                                                        <span class="phone-number">{{$class->Teacher->phone}} </span>
                                                                    </div>
                                                                    <div class="website-content">
                                                                        <span class="contact-title">Email:</span>
                                                                        <span class="contact-website">{{$class->Teacher->email}}</span>
                                                                    </div>

                                                                @else
                                                                    <h5 style="color:silver; text-align: center"> There Is No Teacher Assigned yet </h5>
                                                                @endif



                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->
                    </div>


                    <div class="card">

                        <div class="default-tab">

                            <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"><a href="#students" aria-controls="updates"
                                                                          role="tab" data-toggle="tab">Students</a>
                                </li>
                            </ul>

                            <!-- Tabs -->
                            <div class="tab-content">

                                <!-- Students Tab -->
                                <div role="tabpanel" class="tab-pane active" id="students">

                                    <div class="table-responsive">
                                        <table class="table table-hover student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Names</th>
                                                <th>Gender</th>
                                                <th></th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($class->Students)>0)

				                                <?php $count = 1; ?>

                                                @foreach($class->Students as $student)
                                                    <tr>

                                                        <td>
                                                            #<?php echo $count;?>
                                                        </td>
                                                        <td>
                                                            {{$student->fname}} {{$student->sname}}
                                                        </td>
                                                        <td>
                                                            {{$student->gender}}
                                                        </td>
                                                        {{--<td>--}}
                                                            {{--{{$student->created_at->toDayDateTimeString()}}--}}
                                                        {{--</td>--}}

                                                        <td class="actions">
                                                            {{--<span><a href="{{url('student/'.$student->sid.'/detail')}}"><i class="ti-eye color-default"></i></a> </span>--}}
                                                            {{--<span><a href="{{url('student/'.$student->sid.'/edit')}}"><i class="ti-pencil-alt color-success"></i></a></span>--}}
                                                            {{--<span><a href="#"><i class="ti-trash color-danger"></i> </a></span>--}}
                                                        </td>
                                                    </tr>
					                                <?php $count ++; ?>
                                                @endforeach
                                            @else

                                                <tr>
                                                    <td colspan="3" style="color: silver; text-align: center; margin-top: 30px;"> There are no Students yet </td>
                                                </tr>

                                            @endif




                                            </tbody>
                                        </table>

                                        <div class="pull-right">
                                            {{--{{$applications->links()}}--}}

                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                    </div>


                    <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
