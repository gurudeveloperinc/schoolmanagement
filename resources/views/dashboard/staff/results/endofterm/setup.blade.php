@extends('dashboard.staff.layouts.app')
@section('content')

    <style>
        td input[type=number]{
            width:70px;
        }
    </style>

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">

                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Results</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <div class="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>End Of Term Result Setup</h4>
                                    <div class="card-header-right-icon">
                                        <ul>
                                            <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                            <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="card-body" style="height: 75vh;">
                                    <div class="table-responsive">
                                        <form class="inline">
                                            <div class="form-group">
                                            <label class="col-md-2 control-label">No of times school Opened</label>
                                                <div class="col-md-2">
                                                    <input type="text" class="form-control" name="schoolOpened" >
                                                </div>
                                            </div>
                                            <div class="form-group">
                                            <label class="col-md-2 control-label">Population</label>
                                                <div class="col-md-2">
                                                    <input type="text" class="form-control" name="population" >
                                                </div>
                                            </div>
                                            <button style="margin-left: 50px;" type="submit" class="btn btn-success">Submit</button>
                                        </form>

                                        <form method='post' action="{{url('setup-endofterm-result')}}">
                                            @csrf

                                            <input type="hidden" name="cid" value="{{$class->cid}}">

                                            <table class="table student-data-table m-t-20">
                                                <thead>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>Name</th>
                                                    <th>Class</th>
                                                    <th>Gender</th>
                                                    <th>House</th>
                                                    <th>Population</th>
                                                    <th>Attendance</th>
                                                    <th>School Opened</th>
                                                    <th>Comment</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>


                                                @if(count($students)>0)
					                                <?php $count = 1; ?>
                                                    @foreach($students as $student)
                                                        <tr>
                                                            <td>
								                                <?php echo $count; ?>
                                                            </td>
                                                            <td style="min-width:300px">

                                                                <input type="hidden" name="sids[]" value="{{$student->sid}}">
                                                                <input type="text" name="fnames[]" value="{{$student->fname}}">
                                                                <input type="text" name="snames[]" value="{{$student->sname}}">

{{--                                                                {{$student->fname}} {{$student->sname}}--}}
                                                            </td>
                                                            <td style="min-width:150px">
                                                                @if(isset($student->Class))
                                                                    {{$student->Class->name}}
                                                                @endif
                                                            </td>

                                                            <td>
                                                                <input type="text" name="gender[]" value="{{$student->gender}}">
                                                            </td>

                                                            <td>
                                                                <input type="text" name="house[]" value="{{$student->house}}">
                                                            </td>

                                                            <td>
                                                                {{$population}}
                                                                <input type="hidden"  min="0" max="1000" step="1" name="population[]" value="{{$population}}">

                                                            </td>

                                                            <td>
                                                                <input type="number" name="attendance[]" min="0" max="1000" step="1"
                                                                       @if(isset($student->Results) && count($student->Results) > 0 )
                                                                           value="{{$student->Results[count($student->Results) - 1]->attendance}}"
                                                                       @endif
                                                                />

                                                            </td>

                                                            <td>
                                                                {{$schoolOpened}}
                                                                <input type="hidden" name="schoolOpened[]" min="0" max="1000" value="{{$schoolOpened}}" step="1"/>
                                                            </td>

                                                            <td>
                                                                <select name="comments[]">
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                    <option>3</option>
                                                                    <option>4</option>
                                                                    <option>5</option>
                                                                </select>
                                                            </td>

                                                        </tr>
						                                <?php $count ++; ?>
                                                    @endforeach
                                                @else

                                                    <tr>
                                                        <td colspan="5" style="color: silver; text-align: center; margin-top: 30px;"> There are no students </td>
                                                    </tr>
                                                @endif




                                                </tbody>
                                            </table>


                                            @if($schoolOpened != 0 && $population != 0)
                                            <button type="submit" class="btn btn-success">Submit</button>
                                            @endif
                                        </form>
                                    </div>

                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                </div>
                <!-- /# main content -->
            </div>
            <!-- /# container-fluid -->
        </div>
        <!-- /# main -->
    </div>


@endsection



































