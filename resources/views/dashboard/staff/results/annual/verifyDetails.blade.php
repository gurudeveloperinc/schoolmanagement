@extends('dashboard.staff.layouts.app')
@section('content')

    <style>
        td input{
            width:70px;
        }

        td{
            height:35px;
        }

    </style>



    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Result</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <div id="main-content">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">

                                <table class="result-info" style="width:100%">
                                    <tr>
                                        <td class="whitestroke ">STUDENT NAME: {{strtoupper($student->fname)}} {{strtoupper($student->sname)}} </td>
                                        <td class="whitestroke ">ATTENDANCE</td>
                                        <td class="whitestroke ">
                                            @if(isset($student->Results))
                                                {{$student->Results[count($student->Results) - 1]->attendance}}
                                            @endif
                                        </td>
                                        <td class="whitestroke "></td>
                                    </tr>
                                    <tr>
                                        <td class="whitestroke ">HOUSE: {{strtoupper($student->house)}}</td>
                                        <td class="whitestroke">SCHOOL OPENED</td>
                                        <td class="whitestroke">
                                            @if(isset($student->Results))
                                                {{$student->Results[count($student->Results) - 1]->schoolOpened}}
                                            @endif

                                        </td>
                                        <td class="whitestroke"></td>
                                    </tr>
                                    <tr>
                                        <td class="whitestroke">GENDER: {{strtoupper($student->gender)}}</td>
                                        <td class="whitestroke">HIGHEST CLASS AVERAGE</td>
                                        <td class="whitestroke">{{$highestClassAverage}}</td>
                                        <td class="whitestroke"></td>
                                    </tr>
                                    <tr>
                                        <td class="whitestroke">CLASS: {{$student->Class->name}}</td>
                                        <td class="whitestroke">AVERAGE</td>
                                        <td class="whitestroke">
                                            @if(isset($student->Results))
                                                {{$student->Results[count($student->Results) - 1]->average}}
                                            @endif

                                        </td>
                                        <td class="whitestroke "></td>

                                    </tr>
                                    <tr>
                                        <td class="whitestroke">POPULATION:
                                            @if(isset($student->Results))
                                                {{$student->Results[count($student->Results) - 1]->population}}
                                            @endif

                                        </td>
                                        <td class="whitestroke">TOTAL SCORE</td>
                                        <td class="whitestroke">
                                            @if(isset($student->Results))
                                                {{$student->Results[count($student->Results) - 1]->total}}
                                            @endif
                                        </td>
                                        <td class="whitestroke "></td>
                                    </tr>
                                    <tr>
                                        <td class="whitestroke"></td>
                                        <td class="whitestroke">CLASS POSITION</td>
                                        <td class="whitestroke">
                                        @if(isset($student->Results))
                                            {{$student->Results[count($student->Results) - 1]->position}}
                                        @endif

                                        <!-- position goes here -->
                                        </td>
                                        <td class="whitestroke "></td>

                                    </tr>
                                </table>

                                <p>
                                    @if(isset($student->Results))
                                        Your comment: {{ strtoupper(explode(' ',$student->fname)[0]) }} {{strtoupper($student->Results[count($student->Results) - 1]->tComments)}}

                                <form method="post" action="{{route('results.comment.edit')}}">
                                    @csrf
                                    <input type="hidden" name="rid" value="{{$student->Results[count($student->Results) - 1]->rid}}">
                                    <div class="form-group">
                                        <input type="text" name="comment" class="form-control" value="{{strtoupper($student->Results[count($student->Results) - 1]->tComments)}}">
                                        <button class="btn btn-success" type="submit">Save</button>
                                    </div>
                                </form>
                                @endif
                                </p>
                                <p>

                                    @if(isset($student->Results))
                                        Principal's comment:
                                {{ strtoupper(explode(' ',$student->fname)[0]) }} {{strtoupper($student->Results[count($student->Results) - 1]->hComments)}}
                                <form method="post" action="{{route('results.comment.principal.edit')}}">
                                    @csrf
                                    <input type="hidden" name="rid" value="{{$student->Results[count($student->Results) - 1]->rid}}">
                                    <div class="form-group">
                                        <input type="text" name="comment" class="form-control" value="{{strtoupper($student->Results[count($student->Results) - 1]->hComments)}}">
                                        <button class="btn btn-success" type="submit">Save</button>
                                    </div>
                                </form>

                                @endif
                                </p>


                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-12">

                            <div class="card alert">
                                {{--<div class="card-header pr">--}}
                                {{--<h4>Students list  [{{$student->fname}}]</h4>--}}
                                {{--<div class="search-action">--}}
                                {{--<div class="search-type dib">--}}

                                {{--</div>--}}
                                {{--</div>--}}
                                {{--</div>--}}
                                <div class="card-body">
                                    <div class="table-responsive">

                                        <form method='post' action="{{url('add-subject-results')}}">
                                            @csrf
                                            {{--<input type="hidden" name="subid" value="{{$subject->subid}}">--}}

                                            <table class="table student-data-table m-t-20">
                                                <thead>
                                                <tr>
                                                    <th>S/N</th>
                                                    <th>SUBJECT TITLE</th>
                                                    <th>Target</th>
                                                    {{--<th>CE1 (5)</th>--}}
                                                    {{--<th>CE2 (5)</th>--}}
                                                    {{--<th>AS1 (5)</th>--}}
                                                    {{--<th>CAT1 (10)</th>--}}
                                                    {{--<th>Total (25)</th>--}}
                                                    <th>Total (100)</th>
                                                    <th>Letter Grade</th>
                                                    <th>Remarks</th>
                                                    <th>Target Status</th>
                                                    <th></th>
                                                </tr>
                                                </thead>
                                                <tbody>


                                                @if(count($subjects)>0)
                                                    <?php $count = 1; ?>
                                                    @foreach($scores as $score)
                                                        @if(!empty($score['total']))

                                                            <tr>
                                                                <td>
                                                                    <?php echo $count; ?>
                                                                </td>
                                                                <td>

                                                                    {{$score['subject']}}
                                                                </td>
                                                                <td>
                                                                    {{$target}}
                                                                </td>

                                                                {{--<td>--}}

                                                                {{--{{$subject['result']['ce1']}}--}}
                                                                {{--</td>--}}

                                                                {{--<td>--}}
                                                                {{--{{$subject['result']['ce2']}}--}}
                                                                {{--</td>--}}

                                                                {{--<td>--}}
                                                                {{--{{$subject['result']['as1']}}--}}

                                                                {{--</td>--}}

                                                                {{--<td>--}}
                                                                {{--{{$subject['result']['cat1']}}--}}

                                                                {{--</td>--}}

                                                                {{--<td>--}}
                                                                {{--{{$subject['result']['total']}}--}}
                                                                {{--</td>--}}

                                                                <td>
                                                                    {{$score['total']}}
                                                                </td>

                                                                <td>
                                                                    {{$score['letterGrade']}}
                                                                </td>

                                                                <td>
                                                                    {{$score['remarks']}}
                                                                </td>
                                                                <td style="text-align: center;">
                                                                    {{$score['targetStatus']}}
                                                                </td>
                                                            </tr>

                                                            <?php $count ++; ?>
                                                        @endif
                                                    @endforeach
                                                @else

                                                    <tr>
                                                        <td colspan="5" style="color: silver; text-align: center; margin-top: 30px;"> There are no students </td>
                                                    </tr>
                                                @endif




                                                </tbody>
                                            </table>
                                            {{--<button type="submit" class="btn btn-success">Submit</button>--}}
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
