@extends('dashboard.staff.layouts.app')
@section('content')

    <style>
        td input{
            width:70px;
        }
    </style>



    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Result</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <div id="main-content">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">

                                <div class="card-header pr">
                                    <h4>Verify Class Annual Result </h4>
                                    <div class="search-action">
                                        <div class="search-type dib">

                                        </div>
                                    </div>
                                </div>

                                <div class="card-body">
                                    Population: {{$population}} <br>
                                    Highest Class Average : {{$highestClassAverage}} <br>
                                    School Opened : {{$schoolOpened}} <br>
                                </div>
                            </div>
                        </div>
                    </div>

                            <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-body">
                                    <div class="table-responsive">

                                        <table class="table table-hover student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Names</th>
                                                <th>Gender</th>
                                                <th>Class</th>
                                                <th>Average(%)</th>
                                                <th>Position</th>
                                                <th>Date Generated</th>
                                                <th></th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($students)>0)

			                                    <?php $count = 1; ?>

                                                @foreach($students as $student)
                                                    <tr>

                                                        <td>
						                                    <?php echo $count;?>
                                                        </td>
                                                        <td>
                                                            {{$student->fname}} {{$student->sname}}
                                                        </td>
                                                        <td>
                                                            {{$student->gender}}
                                                        </td>
                                                        <td>
                                                            @if(isset($student->Class))
                                                                {{$student->Class->name}}
                                                            @else
                                                                No Class Assigned
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(isset($student->Results) && count($student->Results) > 0)
                                                                {{$student->Results[count($student->Results) - 1]->average}}
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(isset($student->Results) && count($student->Results) > 0)
                                                                {{$student->Results[count($student->Results) - 1]->position}}
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if(isset($student->Results) && count($student->Results) > 0)
                                                                {{$student->Results[count($student->Results) - 1]->created_at->toDayDateTimeString()}}
                                                            @endif
                                                        </td>


                                                        <td class="actions">
                                                            <span><a class="btn btn-success" href="{{url('verify-annual-result/' . $student->sid)}}">View</a> </span>
                                                            <span><a class="btn btn-success" href="{{url('annual-result-pdf/' . $student->sid)}}">Download PDF</a> </span>
                                                        </td>
                                                    </tr>
				                                    <?php $count ++; ?>
                                                @endforeach
                                            @else

                                                <tr>
                                                    <td style="color: silver; text-align: center; margin-top: 30px;"> There are no Students yet </td>
                                                </tr>

                                            @endif


                                            </tbody>
                                        </table>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
