
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- style-sheet-->
{{--<link rel="stylesheet" href="{{url('results/css/result.css')}}" />--}}
<!-- font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- 525icons -->
    <link rel="stylesheet" type='text/css' href="https://cdn.jsdelivr.net/gh/runestro/525icons@5.5/fonts/525icons.min.css">
    <title>{{$session}} Mock Broad Sheet | Hendon College Abuja</title>
</head>
<body>
<style>
    /*@import url('https://fonts.googleapis.com/css?family=Montserrat:300,500,700');*/
    /* font families need to be inserted  */
    .s-head {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-wrap: wrap;
    }
    body{
        font-family: 'Times New Roman' !important;
    }

    .logoImage{
        width:150px;
        float:left;
        margin:30px;
    }
    .heading {
        font-family: "Montserrat","sans-serif";
        text-align: center;
    }
    h2 {
        font-size: 40px;
        color: #74102D;
        margin-top: 30px;
        margin-bottom: 15px
    }
    h3 {
        font-size: 30px;
        color: #B79443;
        margin-top: 15px;
        margin-bottom: 15px
    }
    h4 {
        font-size: 28px;
        color: #333334;
        font-weight: 500;
        margin-top: 15px;
        margin-bottom: 15px;
    }
    table.result-info {
        empty-cells: show;
        background-color: #B79443;
        border: 1px solid black;
        color: white;
        font-size: 18px;
        font-family: 'Times New Roman';
    }
    table, tr,td {
        border: 1px solid #CCCCCC;
        empty-cells: show;
        border-collapse: collapse;
    }
    td{
        padding-left: 20px;
        text-align: center;
    }
    td.whitestroke{
        border: 1px solid white !important;
        empty-cells: show !important;
        border-collapse: collapse !important;
        text-align: left;
    }
    table.result td {
        border: 1px solid #CCCCCC;
        empty-cells: show;
        font-family: Source Sans Pro;
        font-size: 18px;
    }
    .result tr:nth-child(even){
        background-color: #EFEFEF;
    }
    td.brown{
        background-color: rgba(183, 148, 67, 0.85);
    }
    td.wine{
        background-color: #74102D;
    }
    td.center{
        text-align: center;
    }
    th.center{
        text-align: center;
    }
    td.bold{
        font-weight: 800;
        font-size:14px;
    }
    td.red{
        color: #74102D;
    }
    td.comments{
        font-family: Sue Ellen Francisco;
        color: #4879B3;
        font-size: 14px;
    }
    table.bottom{
        font-size: 20px;
    }
    td.vertical {
        height: 100px;
        padding-left: 0px;
        width: 135px;
        padding-top: 0px;
        padding-right: 0px;
        transform: rotate(270deg);
        text-align: center;

    }
    td.nwidth{
        width: 25px;
    }

</style>
<div align="center" >
    <img src="{{url('/admin/assets/images/logo.png')}}" class="logoImage" alt="school-logo">
    <div class="s-head">
        <div class="heading">
            <h2>HENDON COLLEGE</h2>
            <h3>CO-EDUCATIONAL BOARDING SCHOOL</h3>
            <h4>{{$session}} MOCK END OF TERM BROAD SHEET</h4>
            <h4>{{strtoupper($term)}}</h4>
        </div>
    </div>

    {{--<table class="result-info" style="width:100%">--}}
        {{--<tr>--}}
            {{--<td class="whitestroke ">STAFF NAME: {{strtoupper($subject->Teacher->fname)}} {{strtoupper($subject->Teacher->sname)}} </td>--}}
            {{--<td class="whitestroke">CLASS: {{$class->name}}</td>--}}
            {{--<td class="whitestroke ">SUBJECT: {{strtoupper($subject->name)}}</td>--}}
        {{--</tr>--}}
    {{--</table>--}}

    <table class="result" style="width:100%">
        {{--<tr>--}}
        {{--<td></td>--}}
        {{--<td></td>--}}
        {{--<td></td>--}}
        {{--<td></td>--}}
        {{--<td class="center" colspan="4"><b>COGNITIVE REPORT</b></td>--}}
        {{--<td></td>--}}
        {{--<td></td>--}}
        {{--<td></td>--}}
        {{--</tr>--}}
        <tr style="background-color:white">


            <td class="nwidth"></td>
            <td class="vertical"></td>
            @foreach($subjects as $subject)
            <td class="vertical">{{$subject->name}}</td>
            @endforeach
            <td class="vertical">Total</td>
            <td class="vertical">Average</td>
            <td class="vertical">Grade</td>
            <td class="vertical">Position</td>
            <td class="vertical">Subjects Offered</td>

        </tr>
        <tr style="background-color:#74102D; color:white;">
            <td>S/N</td>
            <td style="min-width:200px;">STUDENT NAME</td>
            @foreach($subjects as $subject)
            <td></td>
            @endforeach

            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>


        @if(count($students)>0)
			<?php $count = 1; ?>
            @foreach($students as $student)
                <tr>
                    <td>
						<?php echo $count; ?>
                    </td>
                    <td>
                        {{$student->fname}} {{$student->sname}}
                    </td>
                    @foreach($subjects as $subject)
                            <td>
                        @foreach($student['subjects'] as $studentSubject)
                            @if($studentSubject['subid'] == $subject->subid)
                                {{$studentSubject['result']['totalOver100']}}
                            @endif
                        @endforeach
                            </td>
                    @endforeach

                    <td>{{number_format($student['total'],2)}}</td>
                    <td>{{number_format($student['average'],2)}}</td>
                    <td>{{$student['letterGrade']}}</td>
                    <td>{{$student['position']}}</td>
                    <td>{{$student['noOfSubjects']}}</td>
                </tr>
				<?php $count ++; ?>
            @endforeach
        @else

            <tr>
                <td colspan="5" style="color: silver; text-align: center; margin-top: 30px;"> There are no students </td>
            </tr>
        @endif



    </table>
    <table class="bottom" style="width:100%">
        <tr>
            <th colspan="2" class="center">KEY TO GRADING</th>
        </tr>
        <tr>
            <td colspan="2" class="center" style="font-size: 16px;">
                [0%- 49% = F = FAIL]   [50%- 59% = E= PROBATION]  [60%-69%= C= CREDIT] [70%-79%= B- = GOOD ][80%-89%= B+ = VERY GOOD ] [90%-100%= A= EXCELLENT]
            </td>
        </tr>
        <tr>
            <td colspan="2" class="center red">KARU DISTRICT, ABUJA</td>
        </tr>
    </table>


</div>
</body>
</html>
