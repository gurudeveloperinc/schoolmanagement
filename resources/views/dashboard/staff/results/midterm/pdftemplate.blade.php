<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    {{--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">--}}
    <!-- style-sheet-->
    {{--<link rel="stylesheet" href="{{url('results/css/result.css')}}" />--}}
    <!-- font awesome -->
    {{--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">--}}
    <!-- 525icons -->
    {{--<link rel="stylesheet" type='text/css' href="https://cdn.jsdelivr.net/gh/runestro/525icons@5.5/fonts/525icons.min.css">--}}
    <title>Result sheet</title>
</head>
<body>
<style>
    /*@import url('https://fonts.googleapis.com/css?family=Montserrat:300,500,700');*/
    /* font families need to be inserted  */
    .s-head {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-wrap: wrap;
    }
    body{
        font-family: 'Times New Roman' !important;
    }

    .logoImage{
        width:100px;
        float:left;
        margin:30px;
    }
    .heading {
        font-family: "Montserrat","sans-serif";
        text-align: center;
    }
    h2 {
        font-size: 30px;
        color: #74102D;
        margin-top: 30px;
        margin-bottom: 15px
    }
    h3 {
        font-size: 20px;
        color: #B79443;
        margin-top: 15px;
        margin-bottom: 15px
    }
    h4 {
        font-size: 18px;
        color: #333334;
        font-weight: 500;
        margin-top: 15px;
        margin-bottom: 15px;
    }
    table.result-info {
        empty-cells: show;
        background-color: #B79443;
        border: 1px solid black;
        color: white;
        font-size: 12px;
        font-family: 'Times New Roman';
    }
    table, tr,td {
        border: 1px solid #CCCCCC;
        empty-cells: show;
        border-collapse: collapse;
    }
    td{
        padding-left: 20px;
        text-align: center;
    }
    td.whitestroke{
        border: 1px solid white !important;
        empty-cells: show !important;
        border-collapse: collapse !important;
        text-align: left;
    }
    table.result td {
        border: 1px solid #CCCCCC;
        empty-cells: show;
        /*font-family: Source Sans Pro;*/
        font-size: 18px;
    }
    .result tr:nth-child(even){
        background-color: #EFEFEF;
    }
    td.brown{
        background-color: rgba(183, 148, 67, 0.85);
    }
    td.wine{
        background-color: #74102D;
    }
    td.center{
        text-align: center;
    }
    th.center{
        text-align: center;
    }
    td.bold{
        font-weight: 800;
    }
    td.red{
        color: #74102D;
    }
    td.comments{
        font-family: Sue Ellen Francisco;
        color: #4879B3;
        font-size: 14px;
    }
    table.bottom{
        font-size: 20px;
    }
    td.vertical {
        height: auto;
        padding-left: 0px;
        width: 25px;
        padding-top: 0px;
        padding-right: 0px;
        transform: rotate(270deg);
        text-align: center;

    }
    td.nwidth{
        width: 25px;
    }

    .container{
        max-width:1024px;
    }

</style>
<div align="center" class="container">
    <img src="admin/assets/images/logo.png" class="logoImage" alt="school-logo">
    <div class="s-head">
        <div class="heading">
            <h2>HENDON COLLEGE</h2>
            <h3>CO-EDUCATIONAL BOARDING SCHOOL</h3>
            <h4>MID TERM CONTINUOUS ASSESSMENT REPORT SHEET</h4>
        </div>
    </div>

    <table class="result-info" style="width:100%">
        <tr>
            <td class="whitestroke ">STUDENT NAME: {{strtoupper($student->fname)}} {{strtoupper($student->sname)}} </td>
            <td class="whitestroke ">ATTENDANCE</td>
            <td class="whitestroke ">
                @if(isset($student->Results))
                    {{$student->Results[count($student->Results) - 1]->attendance}}
                @endif
            </td>
            <td class="whitestroke "></td>
        </tr>
        <tr>
            <td class="whitestroke ">HOUSE: {{strtoupper($student->house)}}</td>
            <td class="whitestroke">SCHOOL OPENED</td>
            <td class="whitestroke">
                @if(isset($student->Results))
                    {{$student->Results[count($student->Results) - 1]->schoolOpened}}
                @endif

            </td>
            <td class="whitestroke"></td>
        </tr>
        <tr>
            <td class="whitestroke">GENDER: {{strtoupper($student->gender)}}</td>
            <td class="whitestroke">HIGHEST CLASS AVERAGE</td>
            <td class="whitestroke">{{$highestClassAverage}}%</td>
            <td class="whitestroke"></td>
        </tr>
        <tr>
            <td class="whitestroke">CLASS: {{$student->Class->name}}</td>
            <td class="whitestroke">AVERAGE</td>
            <td class="whitestroke">
                @if(isset($student->Results))
                    {{$student->Results[count($student->Results) - 1]->average}} %
                @endif

            </td>
            <td class="whitestroke "></td>

        </tr>
        <tr>
            <td class="whitestroke">POPULATION:
                @if(isset($student->Results))
                    {{$student->Results[count($student->Results) - 1]->population}}
                @endif

            </td>
            <td class="whitestroke">TOTAL SCORE</td>
            <td class="whitestroke">
                @if(isset($student->Results))
                    {{$student->Results[count($student->Results) - 1]->total}}
                @endif
            </td>
        </tr>
        {{--<tr>--}}
            {{--<td class="whitestroke"></td>--}}
            {{--<td class="whitestroke">CLASS POSITION</td>--}}
            {{--<td class="whitestroke">14th</td>--}}
            {{--<td class="whitestroke "></td>--}}

        {{--</tr>--}}
    </table>

    <table class="result" style="width:100%">
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="center" colspan="4"><b>COGNITIVE REPORT</b></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
        <tr style="background-color:white">


            <td class="nwidth"></td>
            <td class="vertical"></td>
            <td style="font-size: 12px;" class="vertical">Target (100)</td>
            <td class="vertical">CE1 (5)</td>
            <td class="vertical">CE2 (5)</td>
            <td class="vertical">AS1 (5)</td>
            <td class="vertical">CAT1 (10)</td>
            <td class="vertical">Total (25)</td>
            <td class="vertical">Total (100)</td>
            <td class="vertical">Letter Grade</td>
            <td class="vertical">Remarks</td>
            <td class="vertical">Target Status</td>

        </tr>
        <tr style="background-color:#74102D; color:white;">
            <td>S/N</td>
            <td style="min-width:300px;">SUBJECT TITLE</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>

        @if(count($subjects)>0)
		    <?php $count = 1; ?>
            @foreach($subjects as $subject)
                <tr>
                    <td>
					    <?php echo $count; ?>
                    </td>
                    <td>

                        {{$subject->name}}
                    </td>
                    <td>
                        {{$target}}
                    </td>

                    <td>

                        {{$subject['result']['ce1']}}
                    </td>

                    <td>
                        {{$subject['result']['ce2']}}
                    </td>

                    <td>
                        {{$subject['result']['as1']}}

                    </td>

                    <td>
                        {{$subject['result']['cat1']}}

                    </td>

                    <td>
                        {{$subject['result']['total']}}
                    </td>

                    <td>
                        {{$subject['result']['totalOver100']}}
                    </td>

                    <td>
                        {{$subject['result']['letterGrade']}}
                    </td>

                    <td>
                        {{$subject['result']['remarks']}}
                    </td>
                        @if( $subject['result']['targetStatus'] >= 0 )
                        <td style="background-color: #013220;color:white;">
                            {{$subject['result']['targetStatus']}}
                        </td>
                            @else
                        <td style="background-color: #74102D;color:white;">
                            {{$subject['result']['targetStatus']}}
                        </td>
                    @endif
                 </tr>
			    <?php $count ++; ?>
            @endforeach
        @else

            <tr>
                <td colspan="5" style="color: silver; text-align: center; margin-top: 30px;"> There are no students </td>
            </tr>
        @endif


    </table>
    <table class="bottom" style="width:100%">
        <tr>
            <th colspan="2" class="center">KEY TO GRADING</th>
        </tr>
        <tr>
            <td colspan="2" class="center" style="font-size: 16px;">
                [0%- 49% = F = FAIL]   [50%- 59% = E= PROBATION]  [60%-69%= C= CREDIT] [70%-79%= B- = GOOD ][80%-89%= B+ = VERY GOOD ] [90%-100%= A= EXCELLENT]
            </td>
        </tr>
        <tr>
            <td class="bold">FORM TEACHER’S REMARK:</td>
            <td class="comments">
                @if(isset($student->Results))
                    {{ strtoupper(explode(' ',$student->fname)[0]) }} {{strtoupper($student->Results[count($student->Results) - 1]->tComments)}}
                @endif
            </td>
        </tr>
        <tr>
            <td class="bold">FORM TEACHER’S NAME:</td>
            <td class="bold">
                {{$student->Class->Teacher->fname}}   {{$student->Class->Teacher->sname}}
            </td>
        </tr>
        <tr>
            <td class="bold">PRINCIPAL’S  REMARK:</td>
            <td class="comments">

            </td>
        </tr>
        <tr>
            <td class="bold">PRINCIPAL’S  NAME:</td>
            <td class="bold">KENNETH NNAMANI</td>
        </tr>
        <tr>
            <td style="padding-top:30px; padding-bottom:30px;"class="bold">PRINCIPAL’S SIGNATURE:</td>
            <td class="bold">

            </td>
        </tr>
        <tr>
            <td colspan="2" class="center red">KARU DISTRICT, ABUJA</td>
        </tr>
    </table>








</div>
    </body>
</html>