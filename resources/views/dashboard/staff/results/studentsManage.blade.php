@extends('dashboard.staff.layouts.app')
@section('content')

    <style>
        td input{
            width:70px;
        }
    </style>



    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Result</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header pr">
                                    <h4>Students list  [{{$subject->name}}]</h4>
                                    <div class="search-action">
                                        <div class="search-type dib">
                                            <input class="form-control input-rounded" placeholder="search" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">

                                        <form method='post' action="{{url('add-subject-results')}}">
                                            @csrf
                                            <input type="hidden" name="subid" value="{{$subject->subid}}">

                                        <table class="table student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                <th>Class</th>
                                                <th>CE1 (5)</th>
                                                <th>CE2 (5)</th>
                                                <th>AS1 (5)</th>
                                                <th>CAT1 (10)</th>
                                                <th>CE3 (5)</th>
                                                <th>CE4 (5)</th>
                                                <th>AS2 (5)</th>
                                                <th>CAT2 (10)</th>
                                                <th>PR (10)</th>
                                                <th>EXAM (40)</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>


                                            @if(count($students)>0)
                                                    <?php $count = 1; ?>
                                                @foreach($students as $student)
                                                    <tr>
                                                        <td>
                                                             <?php echo $count; ?>
                                                        </td>
                                                        <td>

                                                            <input type="hidden" name="sids[]" value="{{$student->sid}}">
                                                            {{$student->fname}} {{$student->sname}}
                                                        </td>
                                                        <td>
                                                            @if(isset($student->Class))
                                                            {{$student->Class->name}}
                                                            @endif
                                                        </td>

                                                        <td>
                                                            <input type="number" name="ce1[]" min="0" max="5" step="0.01"
                                                                   @if(isset($student['result']->ce1))
                                                                   value="{{$student['result']->ce1}}"
                                                                   @endif
                                                            />

                                                        </td>

                                                        <td>
                                                            <input type="number" name="ce2[]" min="0" max="5" step="0.01"
                                                                   @if(isset($student['result']->ce2))
                                                                   value="{{$student['result']->ce2}}"
                                                                    @endif
                                                            />
                                                        </td>

                                                        <td>
                                                            <input type="number" name="as1[]" min="0" max="5" step="0.01"
                                                                   @if(isset($student['result']->as1))
                                                                   value="{{$student['result']->as1}}"
                                                                    @endif
                                                            />

                                                        </td>

                                                        <td>
                                                            <input type="number" name="cat1[]" min="0" max="10" step="0.01"
                                                                   @if(isset($student['result']->cat1))
                                                                   value="{{$student['result']->cat1}}"
                                                                    @endif
                                                            />

                                                        </td>

                                                        <td>
                                                            <input type="number" name="ce3[]" min="0" max="5" step="0.01"
                                                                   @if(isset($student['result']->ce3))
                                                                   value="{{$student['result']->ce3}}"
                                                                    @endif
                                                            />

                                                        </td>

                                                        <td>
                                                            <input type="number" name="ce4[]" min="0" max="5" step="0.01"
                                                                   @if(isset($student['result']->ce4))
                                                                   value="{{$student['result']->ce4}}"
                                                                    @endif
                                                            />

                                                        </td>

                                                        <td>
                                                            <input type="number" name="as2[]" min="0" max="5" step="0.01"
                                                                   @if(isset($student['result']->as2))
                                                                   value="{{$student['result']->as2}}"
                                                                    @endif
                                                            />

                                                        </td>

                                                        <td>
                                                            <input type="number" name="cat2[]" min="0" max="10" step="0.01"
                                                                   @if(isset($student['result']->cat2))
                                                                   value="{{$student['result']->cat2}}"
                                                                    @endif
                                                            />

                                                        </td>

                                                        <td>
                                                            <input type="number" name="pr[]" min="0" max="10" step="0.01"
                                                                   @if(isset($student['result']->pr))
                                                                   value="{{$student['result']->pr}}"
                                                                    @endif
                                                            />
                                                        </td>

                                                        <td>
                                                            <input type="number" name="exam[]" min="0" max="40" step="0.01"
                                                                   @if(isset($student['result']->exam))
                                                                   value="{{$student['result']->exam}}"
                                                                    @endif
                                                            />
                                                        </td>

                                                        <td>
                                                            <span><a href="{{url('results/'.$subject->subid.'/'.$student->cid.'/'.$student->sid.'/add')}}"><i class="ti-eye color-default"></i></a> </span>
{{--                                                            <span><a href="{{url('notification/'.$subjects->subid.'/edit')}}"><i class="ti-pencil-alt color-success"></i></a></span>--}}
{{--                                                            <span><a href="{{url('notification/'.$subjects->subid.'/delete')}}"><i class="ti-trash color-danger"></i> </a></span>--}}
                                                        </td>
                                                    </tr>
                                                    <?php $count ++; ?>
                                                @endforeach
                                            @else

                                                <tr>
                                                    <td colspan="5" style="color: silver; text-align: center; margin-top: 30px;"> There are no students </td>
                                                </tr>
                                            @endif




                                            </tbody>
                                        </table>
                                            <button type="submit" class="btn btn-success">Submit</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection