<?php use Carbon\Carbon; ?>
@extends('dashboard.student.layouts.app')
@section('content')
    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Tests</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')
                <div id="main-content">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header pr">
                                    <h4>Manage Tests </h4>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                <th>Available Between</th>
                                                <th>Subject</th>
                                                <th>Max Attempts</th>
                                                <th>Attempt</th>
                                                <th>Score</th>
                                                {{--<th>Classes</th>--}}
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($tests)>0)
								                <?php $count = 1; ?>
                                                @foreach($tests as $test)
                                                    <tr>

                                                        <td>
                                                            # <?php echo $count; ?>
                                                        </td>
                                                        <td>
                                                            {{$test->name}}
                                                        </td>
                                                        <td>
                                                            {{Carbon::createFromFormat("Y-m-d H:i:s",$test->startTime)->toDayDateTimeString()}} and
                                                            {{Carbon::createFromFormat("Y-m-d H:i:s",$test->endTime)->toDayDateTimeString()}}
                                                        </td>

                                                        <td>
                                                            {{$test->Subject->name}}
                                                        </td>
                                                        <td>{{$test->attempts}}</td>
                                                        @if($modifiedTests[$count - 1]['attempt'] > 0)
                                                            <td>
                                                                {{$modifiedTests[$count - 1]['attempt']}}
                                                            </td>
                                                            <td>
                                                                {{$modifiedTests[$count - 1]['score']}} / {{$modifiedTests[$count - 1]['total']}}
                                                            </td>

                                                        @else
                                                            <td colspan="2"> Not attempted Yet</td>
                                                        @endif

                                                        {{--<td>--}}
                                                            {{--@foreach($test->Classes as $item)--}}
                                                                {{--{{$item->name}},--}}
                                                            {{--@endforeach--}}
                                                        {{--</td>--}}
                                                        <td>
                                                            @if($modifiedTests[$count - 1]['attempt'] < $test->attempts )
                                                            <span><a class="btn btn-success " href="{{url('student/test/correction/' . $test->testid)}}">View Correction</a> </span>
                                                            @endif
                                                        </td>
                                                    </tr>
									                <?php $count ++; ?>
                                                @endforeach
                                            @else

                                                <tr>
                                                    <td colspan="7" style="color: silver; text-align: center; margin-top: 30px;"> There are no tests </td>
                                                </tr>

                                            @endif




                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>

                    <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
