<?php use Carbon\Carbon; ?>
@extends('dashboard.student.layouts.app')
@section('content')
    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li>
                                        <a href="#">Dashboard</a>
                                    </li>
                                    <li class="active">Tests</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')
                <div id="main-content">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header pr">
                                    <h4>Correction for {{$test->name}} </h4>
                                </div>
                                <div class="card-body">


                                    @foreach($test->Questions as $question)
                                        @if(isset($question->image))
                                            <div class="card alert">
                                                <div class="card-body" align="center">

                                                    <img style="max-width: 400px;" src="{{$question->image}}">
                                                </div>
                                            </div>
                                        @endif


                                        <div class="card alert">
                                            <div class="card-body">

                                                <h4>Question {{$loop->iteration}}</h4>
                                                <p>{{$question->question}}</p>
                                                <h6>Options</h6>

                                                <div class="table-responsive">
                                                    <table class="table table-borderless student-data-table m-t-20">
                                                        {{--<thead>--}}
                                                        {{--<tr>--}}
                                                        {{--<th>S/N</th>--}}
                                                        {{--<th>Option</th>--}}
                                                        {{--<th></th>--}}
                                                        {{--</tr>--}}
                                                        {{--</thead>--}}
                                                        <tbody>

                                                        <tr>
                                                            <td>
                                                                A
                                                            </td>
                                                            <td>
                                                                {{$question->option1}}
                                                            </td>
                                                            <td>

                                                                @if($answer[$loop->index]->qid == $question->qid && $answer[$loop->index]->option == 1)
                                                                    <span class="badge badge-warning">You selected this</span>
                                                                @endif

                                                                @if($question->correctAnswer == 1)
                                                                    <span class="badge badge-success">
                                                        Correct
                                                        </span>

                                                                @else

                                                                    <span class="badge badge-danger">
                                                            Wrong
                                                        </span>

                                                                @endif
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                B
                                                            </td>
                                                            <td>
                                                                {{$question->option2}}
                                                            </td>
                                                            <td>
                                                                @if($answer[$loop->index]->qid == $question->qid && $answer[$loop->index]->option == 2)
                                                                    <span class="badge badge-warning">You selected this</span>
                                                                @endif

                                                            @if($question->correctAnswer == 2)
                                                                    <span class="badge badge-success">
                                                        Correct
                                                        </span>

                                                                @else

                                                                    <span class="badge badge-danger">
                                                            Wrong
                                                        </span>
                                                                @endif
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                C
                                                            </td>
                                                            <td>
                                                                {{$question->option3}}
                                                            </td>
                                                            <td>
                                                                @if($answer[$loop->index]->qid == $question->qid && $answer[$loop->index]->option == 3)
                                                                    <span class="badge badge-warning">You selected this</span>
                                                                @endif

                                                            @if($question->correctAnswer == 3)
                                                                    <span class="badge badge-success">
                                                        Correct
                                                        </span>

                                                                @else

                                                                    <span class="badge badge-danger">
                                                            Wrong
                                                        </span>
                                                                @endif
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                D
                                                            </td>
                                                            <td>
                                                                {{$question->option4}}
                                                            </td>
                                                            <td>
                                                                @if($answer[$loop->index]->qid == $question->qid && $answer[$loop->index]->option == 4)
                                                                    <span class="badge badge-warning">You selected this</span>
                                                                @endif

                                                            @if($question->correctAnswer == 4)

                                                                    <span class="badge badge-success">
                                                        Correct
                                                        </span>

                                                                @else

                                                                    <span class="badge badge-danger">
                                                            Wrong
                                                        </span>

                                                                @endif
                                                            </td>
                                                        </tr>

                                                        <tr>
                                                            <td>
                                                                E
                                                            </td>
                                                            <td>
                                                                {{$question->option5}}
                                                            </td>
                                                            <td>
                                                                @if($answer[$loop->index]->qid == $question->qid && $answer[$loop->index]->option == 5)
                                                                    <span class="badge badge-warning">You selected this</span>
                                                                @endif

                                                            @if($question->correctAnswer == 5)
                                                                    <span class="badge badge-success">
                                                        Correct
                                                        </span>

                                                                @else

                                                                    <span class="badge badge-danger">
                                                            Wrong
                                                        </span>
                                                                @endif
                                                            </td>
                                                        </tr>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

                                    @endforeach
                                    {{--<div class="table-responsive">--}}
                                    {{--<table class="table student-data-table m-t-20">--}}
                                    {{--<thead>--}}
                                    {{--<tr>--}}
                                    {{--<th>S/N</th>--}}
                                    {{--<th>Question</th>--}}
                                    {{--<th>Image</th>--}}
                                    {{--<th>You Selected</th>--}}
                                    {{--<th>Correct Answer</th>--}}
                                    {{--<th>Status</th>--}}
                                    {{--<th></th>--}}
                                    {{--</tr>--}}
                                    {{--</thead>--}}
                                    {{--<tbody>--}}

                                    {{----}}
                                    {{----}}
                                    {{--@if(count($test->Questions)>0)--}}
                                    {{--<?php $count = 1; ?>--}}
                                    {{--@foreach($test->Questions as $question)--}}
                                    {{--<tr>--}}

                                    {{--<td>--}}
                                    {{--<?php echo $count; ?>--}}
                                    {{--</td>--}}
                                    {{--<td>--}}
                                    {{--{{$question->question}}--}}
                                    {{--</td>--}}
                                    {{--<td>--}}
                                    {{--@if(isset($question->image))--}}
                                    {{--<div class="card alert">--}}
                                    {{--<div class="card-body" align="center">--}}

                                    {{--<img style="max-width: 400px;" src="{{$question->image}}">--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                    {{--@endif--}}

                                    {{--</td>--}}
                                    {{--<td></td>--}}
                                    {{--<td>--}}
                                    {{--@if($question->CorrectAnswer == 1 ) A @endif--}}
                                    {{--@if($question->CorrectAnswer == 2 ) B @endif--}}
                                    {{--@if($question->CorrectAnswer == 3 ) C @endif--}}
                                    {{--@if($question->CorrectAnswer == 4 ) D @endif--}}
                                    {{--@if($question->CorrectAnswer == 5 ) E @endif--}}
                                    {{--</td>--}}
                                    {{--<td>--}}

                                    {{--</td>--}}

                                    {{--</tr>--}}
                                    {{--<?php $count ++; ?>--}}
                                    {{--@endforeach--}}
                                    {{--@else--}}

                                    {{--<tr>--}}
                                    {{--<td colspan="7" style="color: silver; text-align: center; margin-top: 30px;"> There are no questions </td>--}}
                                    {{--</tr>--}}

                                    {{--@endif--}}




                                    {{--</tbody>--}}
                                    {{--</table>--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>

                    <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on
                                    <span id="date-time"></span>
                                    <a href="#" class="page-refresh">Refresh Dashboard</a>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
