<?php use Carbon\Carbon; ?>
@extends('dashboard.student.layouts.app')
@section('content')
    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">

                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Assignments</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>

                @include('notification')

                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header pr">
                                    <h4>Manage Assignments </h4>
                                    <p>These are the assignments for your class. Please view the ones for the subjects you offer.</p>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Name</th>
                                                <th>Due Date</th>
                                                <th>Subject</th>
                                                <th>Class</th>
                                                <th></th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($assignments)>0)
												<?php $count = 1; ?>
                                                @foreach($assignments as $assignment)
                                                    <tr>

                                                        <td>
                                                            <?php echo $count; ?>
                                                        </td>
                                                        <td>
                                                            {{$assignment->name}}
                                                        </td>
                                                        <td>

                                                            {{$assignment->due->toDayDateTimeString()}}
                                                            {{--@if(isset($assignment->due) && Carbon::now()->diffInDays($assignment->due) <= 2)--}}
                                                                {{--<span class="text-danger">{{$assignment->due->toDayDateTimeString()}}</span>--}}
                                                            {{--@endif--}}
                                                            {{--@if(isset($assignment->due) && (Carbon::now()->diffInDays($assignment->due) > 2 && Carbon::now()->diffInDays($assignment->due) <= 6))--}}
                                                                {{--<span class="text-warning">{{$assignment->due->toDayDateTimeString()}}</span>--}}
                                                            {{--@endif--}}

                                                            {{--@if(isset($assignment->due) && Carbon::now()->diffInDays($assignment->due) >= 7 && \Carbon\Carbon::now()->greaterThan($assignment->due))--}}
                                                                {{--<span class="text-success">{{$assignment->due->toDayDateTimeString()}}</span>--}}
                                                            {{--@endif--}}
                                                                {{--@if(isset($assignment->due) && Carbon::now()->diffInDays($assignment->due) >= 7 && \Carbon\Carbon::now()->lessThan(Carbon::createFromFormat("Y-m-d H:i:s",$assignment->due)->diffForHumans()))--}}
                                                                    {{--<span class="text-danger">{{$assignment->due->toDayDateTimeString()}}</span>--}}
                                                                {{--@endif--}}

                                                            @if(!isset($assignment->due))
                                                                No Due Date
                                                            @endif

                                                        </td>
                                                        <td>
                                                            {{$assignment->Subject->name}}
                                                        </td>
                                                        <td>
                                                            {{$assignment->Class->name}}
                                                        </td>
                                                        <td>
                                                            <span><a href="{{url('student/assignment/' . $assignment->aid)}}"><i class="ti-eye color-default"></i></a> </span>
                                                        </td>
                                                    </tr>
													<?php $count ++; ?>
                                                @endforeach
                                            @else

                                                <tr>
                                                    <td colspan="6" style="color: silver; text-align: center; margin-top: 30px;"> There are no assignments </td>
                                                </tr>
                                            @endif




                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /# main content -->
            </div>
            <!-- /# container-fluid -->
        </div>
        <!-- /# main -->
    </div>


@endsection







