<?php use Carbon\Carbon; ?>
@extends('dashboard.student.layouts.app')
@section('content')




    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Assignments</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-body">

                                    {{--@if(empty($test->deadline) || ( !empty($test->deadline) && Carbon::now() < $test->deadline ) )--}}
                                        {{--<a href="{{url('upload-questions/' . $test->testid)}}" class="btn btn-primary pull-right">Upload Questions</a>--}}
                                    {{--@endif--}}


                                    <p class="page-title">Assignment - {{$assignment->name}}</p>
                                    <p>

                                        Description - {{$assignment->description}} <br>
                                        Subject - {{$assignment->Subject->name}} <br>
                                        Due Date - <b>{{Carbon::createFromFormat("Y-m-d H:i:s",$assignment->due)->toDayDateTimeString()}}</b><br> <br>

                                        <a href="{{$assignment->url}}" class="btn btn-success">Download Attached File</a>

                                    </p>
                                </div>
                            </div>


                            <div class="card alert">
                                <div class="card-body">

                                    <h6>Submissions</h6>

                                    @if( count($assignment->Submission) > 0 )

                                        @foreach($assignment->Submission as $submission)
                                     @if(($submission->sid == session()->get('student')->sid) )

                                        Submitted on {{$submission->created_at->toDayDateTimeString()}}
                                            @endif
                                        @endforeach
                                    @else
                                    <form class="inline" enctype="multipart/form-data" method="post" action="{{url('student/assignment/'. $assignment->aid .'/submit')}}">
                                        @csrf

                                        <label>Assignment</label>
                                        <input type="file" class="form-control-file" name="assignment">
                                        <button class="btn btn-success">Submit</button>
                                    </form>

                                @endif
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection
