@extends('dashboard.student.layouts.app')
@section('content')




    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Welcome {{session()->get('student')->fname}} <span>[ {{session()->get('student')->class}} ]</span></h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Home</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="card">
                                <a href="{{url('student/subjects')}}">
                                <div class="stat-widget-eight">
                                    <div class="stat-header">
                                        <div class="header-title pull-left"> My Subjects </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="stat-content">
                                        <div class="pull-left">
                                            <span class="stat-digit"> {{count(session()->get('student')->subjects)}}</span>
                                        </div>
                                        <div class="pull-right">

                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-primary w-70" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                                            <span class="sr-only">70% Complete</span>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>


                        <div class="col-lg-3">
                            <div class="card">
                                <a href="{{url('student/tests')}}">
                                <div class="stat-widget-eight">
                                    <div class="stat-header">
                                        <div class="header-title pull-left"> </div>
                                        <div class="header-title pull-left"> My Tests </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="stat-content">
                                        <div class="pull-left">

                                            <span class="stat-digit"> {{count($tests)}}</span>
                                        </div>
                                        <div class="pull-right">
                                            {{--<span class="progress-stats">70%</span>--}}
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="progress">
                                        <div class="progress-bar progress-bar-warning w-70" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                                            <span class="sr-only">70% Complete</span>
                                        </div>
                                    </div>
                                </div>
                                </a>
                            </div>
                        </div>

                        {{--<div class="col-lg-3">--}}
                            {{--<div class="card">--}}

                                {{--<a href="{{url('student/assignments')}}">--}}
                                    {{--<div class="stat-widget-eight">--}}
                                        {{--<div class="stat-header">--}}
                                            {{--<div class="header-title pull-left"></div>--}}
                                            {{--<div class="header-title pull-left"> My Assignments</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="clearfix"></div>--}}
                                        {{--<div class="stat-content">--}}
                                            {{--<div class="pull-left">--}}

                                                {{--<span class="stat-digit"> 4</span>--}}
                                            {{--</div>--}}
                                            {{--<div class="pull-right">--}}
                                                {{--<span class="progress-stats">70%</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                        {{--<div class="clearfix"></div>--}}
                                        {{--<div class="progress">--}}
                                            {{--<div class="progress-bar progress-bar-success w-70" role="progressbar"--}}
                                                 {{--aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">--}}
                                                {{--<span class="sr-only">70% Complete</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}

{{--                        @if(session()->get('student')->class != "Year 12 / SS 3")--}}

                        <div class="col-lg-3">
                            <div class="card">
{{--                                <a href="{{url('endofterm-result-pdf/' . session()->get('student')->sid )}}">--}}
                                    <div class="stat-widget-eight">
                                        <div class="stat-header">
                                            <div class="header-title pull-left"></div>
                                            <div class="header-title pull-left"> Results</div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="stat-content">
                                            <div class="pull-left">

{{--                                                <span class="btn btn-success"> View End of Term Result</span>--}}
                                                <span class="btn btn-success"> Result Not Released</span>
                                            </div>
                                            {{--<div class="pull-right">--}}
                                                {{--<span class="progress-stats">70%</span>--}}
                                            {{--</div>--}}
                                        </div>
                                        <div class="clearfix"></div>
                                        {{--<div class="progress">--}}
                                            {{--<div class="progress-bar progress-bar-success w-70" role="progressbar"--}}
                                                 {{--aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">--}}
                                                {{--<span class="sr-only">70% Complete</span>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    </div>
{{--                                </a>--}}
                            </div>
                        </div>
{{--                        @endif--}}
{{--                        @if(session()->get('student')->class == "Year 12 / SS 3")--}}

{{--                        <div class="col-lg-3">--}}
{{--                            <div class="card">--}}
{{--                                <a href="{{url('student/end-of-term-result-ss3' )}}">--}}
{{--                                <a href="{{url('midterm-result-pdf/' . session()->get('student')->sid )}}">--}}

{{--                                <div class="stat-widget-eight">--}}
{{--                                        <div class="stat-header">--}}
{{--                                            <div class="header-title pull-left"></div>--}}
{{--                                            <div class="header-title pull-left"> Results</div>--}}
{{--                                        </div>--}}
{{--                                        <div class="clearfix"></div>--}}
{{--                                        <div class="stat-content">--}}
{{--                                            <div class="pull-left">--}}

{{--                                                <span class="btn btn-success"> View Midterm Result</span>--}}
{{--                                                <span class="btn btn-success"> Result Not Released</span>--}}
{{--                                            </div>--}}
{{--                                            --}}{{--<div class="pull-right">--}}
{{--                                                --}}{{--<span class="progress-stats">70%</span>--}}
{{--                                            --}}{{--</div>--}}
{{--                                        </div>--}}
{{--                                        <div class="clearfix"></div>--}}
{{--                                        --}}{{--<div class="progress">--}}
{{--                                            --}}{{--<div class="progress-bar progress-bar-success w-70" role="progressbar"--}}
{{--                                                 --}}{{--aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">--}}
{{--                                                --}}{{--<span class="sr-only">70% Complete</span>--}}
{{--                                            --}}{{--</div>--}}
{{--                                        --}}{{--</div>--}}
{{--                                    </div>--}}
{{--                                </a>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        @endif--}}


                    </div>

                    <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>






@endsection
