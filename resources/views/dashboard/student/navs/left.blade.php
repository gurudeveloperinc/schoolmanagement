
<div class="nano-content">
    <ul>

        <li>
            <img class="img logoImage" src="{{url('admin/assets/images/logo.png')}}"><br>
        </li>


        <li class="label">Home</li>
        <li><a href="{{url('student/dashboard')}}"><i class="ti-view-list-alt"></i> Dashboard</a></li>


        <li class="label">Subjects</li>

        <li><a href="{{url('student/subjects')}}"><i class="ti-view-list-alt"></i> My Subjects</a></li>

        <li class="label">Tests</li>

        <li><a href="{{url('student/tests')}}"><i class="ti-view-list-alt"></i> My Tests</a></li>
        <li><a href="{{url('student/test-history')}}"><i class="ti-view-list-alt"></i>Test History</a></li>

        <li class="label">Assignments</li>

        <li><a href="{{url('student/assignments')}}"><i class="ti-view-list-alt"></i> My Assignments</a></li>



        @if(session()->has('student'))
            @php(  $user = session()->get('student'))
            <li><a href="{{url('logout-user/'.$user->role)}}"><i class="ti-close"></i> Logout</a></li>
        @endif


    </ul>
</div>
