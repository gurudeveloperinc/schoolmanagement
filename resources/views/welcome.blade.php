<!DOCTYPE html>
<html lang="en">
<head>


    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- style-sheet-->
    <link rel="stylesheet" href="{{url('./css/Pre-login.css')}}">
    <!-- font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- 525icons -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/runestro/525icons@5.5/fonts/525icons.min.css">
    <title>Login | Hendon College Portal</title>

</head>
<body>

<div align="center" class="logo">
    <img src="{{url('admin/assets/images/logo.png')}}" alt="school-logo">
</div>

@include('notification');
<div class="top-text">
    <h3>Choose Account type</h3>
</div>
<div class="image-holder">
    <a class="link1" href="{{url('login')}}"> <div class="profile-type image">
            <h3 class="type">Admin</h3>
            <span class="cirle"></span>
        </div></a>
    <a class="link2" href="{{url('parent/login')}}"><div class="profile-type1 image">
            <h3 class="type">Parent</h3>
            <span class="cirle2"></span>
        </div></a>
    <a class="link1" href="{{url('student/login')}}"> <div class="profile-type2 image">
            <h3 class="type">Student</h3>
            <span class="cirle"></span>
        </div></a>
    <a class="link2" href="{{url('staff/login')}}"> <div class="profile-type3 image">
            <h3 class="type">Staff</h3>
            <span class="cirle2"></span>
        </div></a>
</div>
<div class="bottom-text">
    <h6>Hello user!</h6>
    <h6>Please click an icon above to get started</h6>
</div>



<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>


</body>
</html>