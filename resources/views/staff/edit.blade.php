

@extends('layouts.admin')
@section('content')


    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Staff</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <div class="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Edit Staff</h4>
                                </div>
                                <div class="card-body">
                                    <div class="menu-upload-form">
                                        <form class="form-horizontal" method="post" action="{{url('staff/' . $staff->stid .'/edit')}}">
                                            {{csrf_field()}}

                                            <input type="hidden" name="stid" value="{{$staff->stid}}">
                                            <div class="row">

                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Title</label>
                                                            <select name="title" class="form-control bg-ash border-none">

                                                                <option>{{$staff->title}}</option>
                                                                <option >Mr.</option>
                                                                <option >Mrs.</option>
                                                                <option >Dr.</option>
                                                                <option >Prof.</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>First Name</label>
                                                            <input type="text" value="{{$staff->fname}}" class="form-control border-none input-flat bg-ash" name="fname" placeholder=" First Name" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Last Name</label>
                                                            <input type="text" value="{{$staff->sname}}"  name="sname" class="form-control border-none input-flat bg-ash" placeholder="Last Name" required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Designation</label>
                                                            <select name="did" class="form-control bg-ash border-none">
                                                                <option disabled>Select a Staff Designation</option>
                                                                @foreach($designations as $designation)
                                                                    <option
                                                                            @if($designation->did == $staff->did)
                                                                                selected
                                                                            @endif
                                                                            value="{{$designation->did}}">{{$designation->name}}</option>
                                                                @endforeach

                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{--<div class="col-md-3">--}}
                                                {{--<div class="basic-form">--}}
                                                {{--<div class="form-group">--}}
                                                {{--<label>Class</label>--}}
                                                {{--<select name="clid" class="form-control bg-ash border-none">--}}
                                                {{--@foreach($classes as $class)--}}
                                                {{--<option value="{{$class->cid}}">{{$class->name}}</option>--}}
                                                {{--@endforeach--}}
                                                {{--</select>--}}
                                                {{--</div>--}}
                                                {{--</div>--}}
                                                {{--</div>--}}

                                            </div>
                                            <div class="row">


                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Gender*</label>
                                                            <select name="gender"  class="form-control bg-ash border-none" required>
                                                                <option
                                                                @if($staff->gender == 'male') selected @endif
                                                                >Male</option>
                                                                <option
                                                                        @if($staff->gender == 'female') selected @endif
                                                                >Female</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Date of Birth</label>
                                                            <input type="date" name="dob" value="{{$staff->dob}}" class="form-control calendar bg-ash" placeholder="dd/mm/yyyy" id="text-calendar" >
                                                            <span class="ti-calendar form-control-feedback booking-system-feedback m-t-30"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Phone</label>
                                                            <input type="tel" value="{{$staff->phone}}" name="phone" class="form-control border-none input-flat bg-ash" placeholder="Phone Number" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Nationality</label>
                                                            <input type="text" value="{{$staff->nationality}}" name="nationality"  class="form-control border-none input-flat bg-ash" placeholder="Nationality" >
                                                        </div>
                                                    </div>
                                                </div>
                                                {{--<div class="col-md-3">--}}
                                                {{--<div class="basic-form">--}}
                                                {{--<div class="form-group">--}}
                                                {{--<label>ID No</label>--}}
                                                {{--<input type="text" class="form-control border-none input-flat bg-ash" placeholder="">--}}
                                                {{--</div>--}}
                                                {{--</div>--}}
                                                {{--</div>--}}

                                            </div>
                                            <div class="row">

                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>State of  Origin</label>
                                                            <input type="text" value="{{$staff->stateOfOrigin}}" name="stateOfOrigin" class="form-control border-none input-flat bg-ash" placeholder="State Of Origin" >
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-9">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Email Address</label>
                                                            <input type="email" name="email" value="{{$staff->email}}" class="form-control border-none input-flat bg-ash" placeholder="Enter email" required>
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Staff Bio.</label>
                                                            <textarea type="text" name="bio" class="form-control border-none input-flat bg-ash" placeholder="Enter Some information About the Staff" rows="3">{{$staff->bio}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Address</label>
                                                            <textarea type="text" name="residentialAddress" class="form-control border-none input-flat bg-ash" placeholder="Home Address" rows="5" >{{$staff->residentialAddress}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="basic-form">
                                                        <div class="form-group image-type">
                                                            <label>Upload Teacher Photo <span>(150 X 150)</span></label>
                                                            <input type="file"  name="image" accept="image/*">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <button class="btn btn-default btn-lg m-b-10 bg-warning border-none m-r-5 sbmt-btn" type="submit">Save</button>
                                            <button class="btn btn-default btn-lg m-b-10 m-l-5 sbmt-btn" type="button">Reset</button>


                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                </div>
                <!-- /# main content -->
            </div>
            <!-- /# container-fluid -->
        </div>
        <!-- /# main -->
    </div>

@endsection