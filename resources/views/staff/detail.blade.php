
@extends('layouts.admin')
@section('content')

    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Staff</li>
                                    <li>Details</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <div id="main-content">
                    <!-- /# column -->

                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="card alert">
                                <div class="card-header">

                                </div>
                                <div class="card-body">
                                    <div class="default-tab">
                                        <ul class="nav nav-tabs" role="tablist">
                                            <li role="presentation" class="active"><a href="#home1" aria-controls="home" role="tab" data-toggle="tab">Staff</a></li>
                                            {{--<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Comments</a></li>--}}
                                            {{--<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Results</a></li>--}}
                                            {{--<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab"> Finance</a></li>--}}
                                        </ul>
                                        <div class="tab-content">
                                            <div role="tabpanel" class="tab-pane active" id="home1">

                                                {{--first card--}}
                                                <div class="card-body">
                                                    <div class="user-profile m-t-15">
                                                        <div class="row">
                                                            <div class="col-lg-4">
                                                                <div class="user-photo m-b-30">
{{--                                                                    <img class="img-responsive" src="{{$student->image}}" alt="Photo Space" />--}}
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-8">
                                                                <div class="user-profile-name dib">{{$staff->title}}  {{strtoupper($staff->fname)}} {{strtoupper($staff->sname)}}</div>
                                                                <div class="useful-icon dib pull-right">
                                                                    <span><a href="{{url('staff/' . $staff->stid . '/edit')}}"><i class="ti-pencil-alt"></i></a> </span>
                                                                </div>
                                                                <div class="custom-tab user-profile-tab">
                                                                    <ul class="nav nav-tabs" role="tablist">
                                                                        <li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">About</a></li>
                                                                    </ul>
                                                                    <div class="tab-content">
                                                                        <div role="tabpanel" class="tab-pane active" id="1">
                                                                            <div class="contact-information">
                                                                                <div class="phone-content">
                                                                                    <span class="contact-title">Staff ID:</span>
                                                                                    <span class="phone-number">{{$staff->stid}}</span>
                                                                                </div>
                                                                                <div class="phone-content">
                                                                                    <span class="contact-title">Designation</span>
                                                                                    <span class="phone-number">{{$staff->Designation->name}}</span>
                                                                                </div>
                                                                                <div class="phone-content">
                                                                                    <span class="contact-title">Category</span>
                                                                                    <span class="phone-number">{{$staff->Designation->category}}</span>
                                                                                </div>
                                                                                <div class="website-content">
                                                                                    <span class="contact-title">Class In Charge:</span>

                                                                                    @if(isset($staff->Class))
                                                                                        <span class="contact-website">{{$staff->Class->name}}</span>
                                                                                        @else
                                                                                        <span class="contact-website">Not Assigned to any class</span>

                                                                                    @endif
                                                                                </div>

                                                                                <div class="gender-content">
                                                                                    <span class="contact-title">Gender:</span>
                                                                                    <span class="gender">{{ title_case( $staff->gender) }}</span>
                                                                                </div>
                                                                                <div class="phone-content">
                                                                                    <span class="contact-title">Phone</span>
                                                                                    <span class="phone-number">{{$staff->phone}}</span>
                                                                                </div>

                                                                                <div class="birthday-content">
                                                                                    <span class="contact-title">Date of Birth:</span>
                                                                                    <span class="birth-date">{{$staff->dob}} </span>
                                                                                </div>


                                                                                <div class="email-content">
                                                                                    <span class="contact-title">Email:</span>
                                                                                    <span class="contact-email">{{$staff->email}}</span>
                                                                                </div>

                                                                                <div class="email-content">
                                                                                    <span class="contact-title">Nationality:</span>
                                                                                    <span class="contact-email">{{$staff->nationality}}</span>
                                                                                </div>


                                                                                <div class="address-content">
                                                                                    <span class="contact-title">Home Address:</span>
                                                                                    <span class="mail-address">{{$staff->residentialAddress}}</span>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {{--end of first card--}}


                                            </div>





                                            <div role="tabpanel" class="tab-pane" id="profile">
                                                <p>
                                                    {{--This is for comments made by parents to the staff--}}
                                                </p>


                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="messages">
                                                <p>This holds a report of the academic performance of the student,complaints and any suggestion for academics</p>
                                            </div>
                                            <div role="tabpanel" class="tab-pane" id="settings">
                                                <p>This tab holds the financial record of the students payments and pending payments.</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    <!-- /# column -->
                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection









