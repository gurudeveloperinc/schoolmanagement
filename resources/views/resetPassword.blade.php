<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{url('css/bootstrap.min.css')}}" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- style-sheet-->
    <link rel="stylesheet" href="{{url('./css/staff.css')}}">
    <!-- font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- 525icons -->
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/gh/runestro/525icons@5.5/fonts/525icons.min.css">
    <title>Staff Login</title>

    <!-- Standard -->
    <link rel="shortcut icon" href="{{url('./assets/logo.png')}}">


</head>

<body>

<div class="container-fluid">
    <div class="image-holder2 d-lg-none d-xl-none">

    </div>

    <div class="login">
        <div align="center" class="logo">
            <a href="{{url('/')}}">
                <img src="{{url('./assets/logo.png')}}" alt="school-logo">
            </a>
        </div>
        <div class="form-holder">
            @include('notification')

            @if(isset($change) && $change == true)

            <form method="post" action="{{url('reset-password')}}">
                {{csrf_field()}}

                <input type="hidden" name="email" value="{{$email}}">
                <input type="hidden" name="role" value="{{$role}}">

                <div class="email tags">
                    <i class="fas fa-lock icon"></i>
                    <input class="input" placeholder="New Password" name="newPassword" type="password">
                    <hr>
                </div>
                <div class="password tags">
                    <i class="fas fa-lock icon"></i>
                    <input class="input" placeholder="Confirm new password" name="confirmPassword" type="password">
                    <hr>
                </div>

                <button type="submit" class="btn">Reset</button>
            </form>

            @endif
        </div>
    </div>
    <div class="image-holder">

    </div>
</div>
<!-- <div  class="a-img"></div> -->




<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>



</body>

</html>
