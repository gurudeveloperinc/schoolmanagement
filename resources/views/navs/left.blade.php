<div class="nano-content">
    <ul>

        <li>
            <img class="img logoImage" src="{{url('admin/assets/images/logo.png')}}" ><br>
        </li>

        <li class="label">Home</li>
        <li><a href="{{url('/home')}}"><i class="ti-view-list-alt"></i> DashBoard</a></li>



        <li class="label">Admissions</li>
        <li class="active"><a class="sidebar-sub-toggle"><i class="ti-home"></i> Applications  <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('create-application')}}">  Add </a></li>
                <li><a href="{{url('upload-application')}}">  Upload </a></li>
                <li><a href="{{url('manage-applications')}}"> Manage</a></li>
            </ul>
        </li>

        <li class="label">Students Data</li>
        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i> Student <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('/students')}}">View Students</a></li>
            </ul>
        </li>




        <li class="label">Parents Data</li>
        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i> Parent <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('parents')}}">View Parents</a></li>

            </ul>
        </li>


        <li class="label">Academics</li>
        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i> Focus <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('add-focus')}}">Add</a></li>
                <li><a href="{{url('focus')}}">Manage</a></li>

            </ul>
        </li>

        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i> Subjects <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('add-subjects')}}">Add</a></li>
                <li><a href="{{url('subjects')}}">Manage</a></li>

            </ul>
        </li>
        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i> Classes <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('add-class')}}">Add</a></li>
                <li><a href="{{url('classes')}}">Manage</a></li>

            </ul>
        </li>

        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i> Tests <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('admin/add-test')}}">Add</a></li>
                {{--<li><a href="{{url('add-single-test')}}">Add</a></li>--}}
                <li><a href="{{url('tests')}}">Manage</a></li>

            </ul>
        </li>

        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i> Result Comments <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('admin/result/comment/add')}}">Add</a></li>
                <li><a href="{{url('admin/result/comments')}}">Manage</a></li>

            </ul>
        </li>





        <li class="label">Human Resource</li>
        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i> Staff <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('add-staff')}}">Add</a></li>
                <li><a href="{{url('staff/upload')}}">Upload</a></li>
                <li><a href="{{url('manage-staff')}}">Manage</a></li>

            </ul>
        </li>

        <li><a class="sidebar-sub-toggle"><i class="ti-pencil-alt"></i> Designation <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('add-designation')}}">Add</a></li>
                <li><a href="{{url('designations')}}">Manage</a></li>

            </ul>
        </li>


        <li class="label"> Extras</li>
        <li><a class="sidebar-sub-toggle"><i class="ti-cup"></i> Notifications <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('notification')}}">Add</a></li>
                <li><a href="{{url('view-notifications')}}">Manage</a></li>
            </ul>
        </li>

        <li><a class="sidebar-sub-toggle"><i class="ti-cup"></i> Settings <span class="sidebar-collapse-icon ti-angle-down"></span></a>
            <ul>
                <li><a href="{{url('settings')}}">Manage</a></li>
            </ul>
        </li>

        <li><a href="{{url('logout')}}"><i class="ti-close"></i> Logout</a></li>
    </ul>
</div>
