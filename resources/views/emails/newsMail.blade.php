<html>
<body>

<h3>{{$news->title}}</h3> <br>

<p>
    {!! $news->content !!}
</p>

@if(count($news->Images) > 0)

    @foreach($news->Images as $image)
    <img src="{{$image->url}}" style="width:30%; height: auto;margin-left: 40px; float: left;">
    @endforeach
@endif

</body>
</html>
