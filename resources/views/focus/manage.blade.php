@extends('layouts.admin')
@section('content')




    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Dashboard</h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Focus</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                @include('notification')

                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header pr">
                                    <h4>Focus </h4>
                                    <div class="search-action">
                                        <div class="search-type dib">
                                            <input class="form-control input-rounded" placeholder="Search" type="text">
                                        </div>
                                    </div>
                                    <div class="card-header-right-icon">
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="table-responsive">
                                        <table class="table student-data-table m-t-20">
                                            <thead>
                                            <tr>
                                                <th>S/N</th>
                                                <th>Title</th>
                                                <th>Created</th>
                                                <th></th>

                                            </tr>
                                            </thead>
                                            <tbody>

                                            @if(count($focus)>0)

												<?php $count = 1; ?>

                                                @foreach($focus as $f)
                                                    <tr>

                                                        <td>
                                                            <?php echo $count;?>
                                                        </td>
                                                        <td>
                                                            {{$f->name}}
                                                        </td>
                                                        <td>
                                                            {{$f->created_at->diffForHumans()}}
                                                        </td>

                                                        <td>
                                                            <span><a href="{{url('focus/' . $f->fid)}}"><i class="ti-eye color-default"></i></a> </span>
                                                            {{--<span><a href="{{url('notification/'.$subjects->subid.'/edit')}}"><i class="ti-pencil-alt color-success"></i></a></span>--}}
                                                            {{--<span><a href="{{url('notification/'.$subjects->subid.'/delete')}}"><i class="ti-trash color-danger"></i> </a></span>--}}
                                                        </td>
                                                    </tr>
													<?php $count ++; ?>
                                                @endforeach
                                            @else

                                                <tr>
                                                    <td colspan="4" style="color: silver; text-align: center; margin-top: 30px;"> There are no focus areas yet </td>
                                                </tr>
                                            @endif




                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /# column -->

                    </div>
                    <!-- /# row -->

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection