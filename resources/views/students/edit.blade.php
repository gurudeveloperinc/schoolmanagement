

@extends('layouts.admin')
@section('content')


    <div class="content-wrap">
        <div class="main">
            {{--@include('notification')--}}

            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Student</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
{{--                        <a href="{{url('student/' . $student->sid . '/detail')}}" class="btn btn-secondary padding-overlay pull-left"> Back </a>--}}
                        <a href="{{url('students')}}" class="btn btn-secondary padding-overlay pull-left"> Back </a>
                    </div>
                </div>

                @include('notification')

                <div class="main-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="card alert">
                                <div class="card-header">
                                    <h4>Update Student</h4>
                                </div>
                                <div class="card-body">
                                    <div class="menu-upload-form">
                                        <form class="form-horizontal" enctype="multipart/form-data" method="post" action="{{url('student/'.$student->sid . '/edit')}}">
                                            {{csrf_field()}}
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>First Name</label>
                                                            <input type="text" class="form-control border-none input-flat bg-ash" name="fname" disabled value="{{$student->fname}}" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Last Name</label>
                                                            <input type="text" value="{{$student->sname}}" name="sname" class="form-control border-none input-flat bg-ash" placeholder="" disabled>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Class</label>
                                                            <select name="cid" class="form-control bg-ash border-none">
                                                                <option value="">None</option>
                                                                @foreach($classes as $class)
                                                                <option  @if($student->cid == $class->cid ) selected="selected" @endif  value="{{$class->cid}}">{{$class->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Religion</label>
                                                            <input type="text" name="religion" value="{{$student->religion}}" class="form-control border-none input-flat bg-ash" placeholder="">
                                                            {{--<select name="religion" class="form-control bg-ash border-none">--}}

                                                                {{--<option>Muslim</option>--}}
                                                                {{--<option>Christian</option>--}}
                                                                {{--<option>Traditional</option>--}}
                                                                {{--<option>Hinduism</option>--}}
                                                                {{--<option>Buddhist</option>--}}
                                                                {{--<option>Atheist</option>--}}
                                                                {{--<option>Other</option>--}}
                                                            {{--</select>--}}
                                                        </div>
                                                    </div>
                                                </div>
                                                {{--<div class="col-md-3">--}}
                                                    {{--<div class="basic-form">--}}
                                                        {{--<div class="form-group">--}}
                                                            {{--<label>Arm</label>--}}
                                                            {{--<select class="form-control bg-ash border-none">--}}

                                                                {{--<option selected>A</option>--}}
                                                                {{--<option>B</option>--}}
                                                                {{--<option>C</option>--}}
                                                                {{--<option>D</option>--}}
                                                                {{--<option>E</option>--}}
                                                            {{--</select>--}}
                                                        {{--</div>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Gender*</label>
                                                            <select name="gender" class="form-control bg-ash border-none">
                                                                <option @if($student->gender == 'Male') selected="selected" @endif >Male</option>
                                                                <option @if($student->gender == 'Female') selected="selected" @endif >Female</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Date of Birth</label>
                                                            <input type="text" pattern="[0-9][0-9]-[0-9][0-9]-[0-9][0-9][0-9][0-9]" class="form-control bg-ash" name="dob" value="{{$student->dob}}" placeholder="dd/mm/yyyy">
                                                            <span class="ti-calendar form-control-feedback booking-system-feedback m-t-30"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>ID No</label>
                                                            <input type="text" name="studentid" value="{{$student->studentid}}" class="form-control border-none input-flat bg-ash" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Focus</label>
                                                            <select name="fid" class="form-control bg-ash border-none">
                                                                @foreach($focuses as $focus)
                                                                <option value="{{$focus->fid}}">{{$focus->name}}</option>
                                                                @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Nationality</label>
                                                            <input type="text" name="nationality" value="{{$student->nationality}}" class="form-control border-none input-flat bg-ash" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>E-mail</label>
                                                            <input type="text" name="email" value="{{$student->email}}" class="form-control border-none input-flat bg-ash" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Phone</label>
                                                            <input type="text" name="phone" value="{{$student->phone}}" class="form-control border-none input-flat bg-ash" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Password</label>
                                                            <input type="text" name="password" class="form-control border-none input-flat bg-ash" placeholder="">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">

                                                <div class="col-md-12">
                                                    <div class="basic-form">
                                                        <div class="form-group">
                                                            <label>Address</label>
                                                            <textarea type="text" name="address"  class="form-control border-none input-flat bg-ash" placeholder="" rows="5">{{$student->address}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="basic-form">
                                                        <div class="form-group image-type">
                                                            <label>Upload Photo <span style="font-size: small">(Optional)</span></label>
                                                            <input type="file"  name="image" accept="image/*">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>


                                            <button class="btn btn-default btn-lg m-b-10 bg-success border-none m-r-5 sbmt-btn" type="submit">Save</button>

                                        </form>
                                    </div>
                                </div>
                            </div>
                            <!-- /# card -->
                        </div>
                        <!-- /# column -->
                    </div>
                    <!-- /# row -->
                </div>
                <!-- /# main content -->
            </div>
            <!-- /# container-fluid -->
        </div>
        <!-- /# main -->
    </div>





@endsection