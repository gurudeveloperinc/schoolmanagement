@extends('layouts.admin')

@section('content')



    <div class="content-wrap">
        <div class="main">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-8 p-r-0 title-margin-right">
                        <div class="page-header">
                            <div class="page-title">
                                <h1>Hello, <span>{{auth()->user()->name}}</span></h1>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                    <div class="col-lg-4 p-l-0 title-margin-left">
                        <div class="page-header">
                            <div class="page-title">
                                <ol class="breadcrumb text-right">
                                    <li><a href="#">Dashboard</a></li>
                                    <li class="active">Home</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <!-- /# column -->
                </div>
                <!-- /# row -->
                <div id="main-content">
                    <div class="row">
                        <div class="col-lg-3">
                            <a href="{{url('manage-staff')}}">
                                <div class="card">
                                    <div class="stat-widget-eight">
                                        <div class="stat-header">
                                            <div class="header-title pull-left"> Staff </div>
                                            <div class="card-option drop-menu pull-right"><i class="ti-more-alt" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="link"></i>
                                                <ul class="card-option-dropdown dropdown-menu">
                                                    <li><a href="#"><i class="ti-loop"></i> Update data</a></li>
                                                    <li><a href="#"><i class="ti-menu-alt"></i> Detail log</a></li>
                                                    <li><a href="#"><i class="ti-pulse"></i> Statistics</a></li>
                                                    <li><a href="#"><i class="ti-power-off"></i> Clear ist</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="stat-content">
                                            <div class="pull-left">
                                                {{--<i class="ti-arrow-up color-success"></i>--}}
                                                <span class="stat-digit"> {{count($staff)}}</span>
                                            </div>
                                            <div class="pull-right">
                                                {{--<span class="progress-stats">70%</span>--}}
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-primary w-70" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">70% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3">
                            <a href="{{url('subjects')}}">
                                <div class="card">
                                    <div class="stat-widget-eight">
                                        <div class="stat-header">
                                            <div class="header-title pull-left"> </div>
                                            <div class="header-title pull-left"> Subjects </div>
                                            <div class="card-option drop-menu pull-right"><i class="ti-more-alt" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="link"></i>
                                                <ul class="card-option-dropdown dropdown-menu">
                                                    <li><a href="#"><i class="ti-loop"></i> Update data</a></li>
                                                    <li><a href="#"><i class="ti-menu-alt"></i> Detail log</a></li>
                                                    <li><a href="#"><i class="ti-pulse"></i> Statistics</a></li>
                                                    <li><a href="#"><i class="ti-power-off"></i> Clear ist</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="stat-content">
                                            <div class="pull-left">
                                                {{--<i class="ti-arrow-up color-success"></i>--}}
                                                <span class="stat-digit"> {{count($subjects)}}</span>
                                            </div>
                                            <div class="pull-right">
                                                {{--<span class="progress-stats">70%</span>--}}
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-success w-70" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">70% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3">
                            <a href="{{url('students')}}">
                                <div class="card">
                                    <div class="stat-widget-eight">
                                        <div class="stat-header">
                                            <div class="header-title pull-left"> Students</div>
                                            <div class="card-option drop-menu pull-right"><i class="ti-more-alt" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="link"></i>
                                                <ul class="card-option-dropdown dropdown-menu">
                                                    <li><a href="#"><i class="ti-loop"></i> Update data</a></li>
                                                    <li><a href="#"><i class="ti-menu-alt"></i> Detail log</a></li>
                                                    <li><a href="#"><i class="ti-pulse"></i> Statistics</a></li>
                                                    <li><a href="#"><i class="ti-power-off"></i> Clear ist</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="stat-content">
                                            <div class="pull-left">
                                                {{--<i class="ti-arrow-down color-danger"></i>--}}
                                                <span class="stat-digit"> {{count($students)}}</span>
                                            </div>
                                            <div class="pull-right">
                                                {{--<span class="progress-stats">70%</span>--}}
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-warning w-70" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">70% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3">
                            <a href="{{url('parents')}}">
                              <div class="card">
                                    <div class="stat-widget-eight">
                                        <div class="stat-header">
                                            <div class="header-title pull-left">Parents</div>
                                            <div class="card-option drop-menu pull-right"><i class="ti-more-alt" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="link"></i>
                                                <ul class="card-option-dropdown dropdown-menu">
                                                    <li><a href="#"><i class="ti-loop"></i> Update data</a></li>
                                                    <li><a href="#"><i class="ti-menu-alt"></i> Detail log</a></li>
                                                    <li><a href="#"><i class="ti-pulse"></i> Statistics</a></li>
                                                    <li><a href="#"><i class="ti-power-off"></i> Clear ist</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="stat-content">
                                            <div class="pull-left">
                                                {{--<i class="ti-arrow-down color-danger"></i>--}}
                                                <span class="stat-digit"> {{count($parents)}}</span>
                                            </div>
                                            <div class="pull-right">
                                                {{--<span class="progress-stats">70%</span>--}}
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-success w-70" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">70% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-lg-3">
                            <a href="{{url('manage-applications')}}">
                                <div class="card">
                                        <div class="stat-widget-eight">
                                        <div class="stat-header">
                                            <div class="header-title pull-left"> Applications </div>
                                            <div class="card-option drop-menu pull-right"><i class="ti-more-alt" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="link"></i>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="stat-content">
                                            <div class="pull-left">
                                                <span class="stat-digit"> {{count($pendingApplications)}} pending</span>
                                                <small>({{count($applications)}} in total)</small>
                                            </div>
                                            <div class="pull-right">
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-primary w-70" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100">
                                                <span class="sr-only">70% Complete</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                    </div>
                        <!-- /# row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="footer">
                                <p>This dashboard was generated on <span id="date-time"></span> <a href="#" class="page-refresh">Refresh Dashboard</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection