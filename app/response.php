<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class response extends Model
{
	protected $primaryKey = 'resid';
    protected $table = 'responses';

	public function Question() {
		return $this->belongsTo(question::class,'qid','qid');
    }

	public function Attempt() {
		return $this->belongsTo(attempt::class,'atid','atid');
    }
}
