<?php

namespace App\Http\Controllers;

use App\attempt;
use App\question;
use App\response;
use App\testResult;
use Illuminate\Http\Request;

class WebApiController extends Controller
{
	public function addAttempt(Request $request) {
		$testid = $request->input('testid');
		$sid = $request->input('sid');

		$attempt = new attempt();
		$attempt->sid = $sid;
		$attempt->testid = $testid;
		$attempt->save();

		return array("message" => "Success","data" => $attempt);
    }


	public function test(Request $request) {
		return $request->all();
    }


	public function addTestResult( Request $request ) {

		$sid = $request->input('sid');
		$testid = $request->input('testid');
		$total = $request->input('total');
		$questions = $request->input('questions');
		$answers = $request->input('answers');
		$attempt = attempt::where('testid',$testid)->where('sid',$sid)->count();

		$correctAnswers = 0;

		for($i = 0; $i < count($questions); $i++){
			$response = new response();
			$response->sid = $sid;
			$response->qid = $questions[$i]['qid'];
			$response->option =  $answers[$i];
			$response->attempt = $attempt;
			$response->save();

			$question = question::find($questions[$i]['qid']);

			if($question->correctAnswer == $answers[$i]){
				$correctAnswers++;
			}
		}


		$testResult = new testResult();
		$testResult->testid = $testid;
		$testResult->sid = $sid;
		$testResult->score = $correctAnswers;
		$testResult->attempt = $attempt;
		$testResult->total = $total;
		$testResult->save();

		return $correctAnswers;
    }
}
