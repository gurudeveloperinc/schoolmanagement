<?php

namespace App\Http\Controllers;

use App\assignment;
use App\assignmentSubmission;
use App\attempt;
use App\classes;
use App\focus;
use App\focusSubject;
use App\response;
use App\result;
use App\setting;
use App\sickbay;
use App\student;
use App\subjectResult;
use App\subjects;
use App\subjectStudent;
use App\test;
use App\testClass;
use App\testResult;
use Carbon\Carbon;
use Illuminate\Http\Request;

class StudentController extends Controller
{

	public function dashboard(  ) {
		if (session()->get('student') != null):
			$student = session()->get('student');
		else:
			return redirect('student/login');
		endif;

		$subjects = $student->Subjects;
		$subIDs = array();
		foreach($subjects as $subject){
			array_push($subIDs,$subject->subid);
		}

		$testClass = testClass::where('cid',$student->cid)->get();
		$testIDs = array();

		foreach($testClass as $item){
			$test =  test::find($item->testid);
			if(count($test->Questions) > 0 ) // check if there are questions in the test
				array_push($testIDs, $item->testid);
		}

		$tests = test::whereIn('testid',$testIDs)->where('endTime','>=',Carbon::now())->get();




		return view('dashboard.student.dashboard',[
			'student' => $student,
			'tests'   => $tests
		]);

	}

	public function endOfTermResultSS3() {

		$sid = session()->get('student')->sid;

		$student = student::find($sid);

		$subids = array();

		$subjectResults = subjectResult::where('sid',$student->sid)->get();

		foreach($subjectResults as $subjectResult){
			array_push($subids,$subjectResult->subid);
		}
		$subjects = subjects::whereIn("subid",$subids)->get();

		//get batch number
		if(result::where('cid',$student->cid)->count() > 0){
			$batch = result::where('cid',$student->cid)->get()->last()->batch;
			if(!isset($batch) || empty($batch)) $batch = 0;
		} else $batch = 0;


		$highestClassAverage = result::where('cid',$student->cid)->where('batch',$batch)->orderBy('average','desc')->get()->first()->average;

//		$highestClassAverage = result::where('cid',$student->cid)->orderBy('average','desc')->get()->first()->average;
		$target = 60;

		foreach($subjects as $subject){
			$result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->get()->last();

//			$totalScore = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1;
//			$total = 5 + 5 + 5 + 10;
			$totalOver100 =  ($result->exam / 40 ) * 100;
			$letterGrade = '';
			$remark = '';
			$targetStatus = $totalOver100 - $target;

			switch ($totalOver100){
				case $totalOver100 < 50:
					$letterGrade = 'F';
					$remark = 'FAIL';
					break;
				case $totalOver100 >= 50 && $totalOver100 < 60:
					$letterGrade = 'E';
					$remark = 'PROBATION';
					break;
				case $totalOver100 >= 60 && $totalOver100 < 70:
					$letterGrade = 'C';
					$remark = 'CREDIT';
					break;
				case $totalOver100 >= 70 && $totalOver100 < 80:
					$letterGrade = 'B';
					$remark = 'GOOD';
					break;
				case $totalOver100 >= 80 && $totalOver100 < 90:
					$letterGrade = 'B+';
					$remark = 'VERY GOOD';
					break;
				case $totalOver100 >= 90 && $totalOver100 <= 100:
					$letterGrade = 'A';
					$remark = 'EXCELLENT';

			}

			$resultItem = array(
//				'ce1' => $result->ce1,
//				'ce2' => $result->ce2,
//				'as1' => $result->as1,
//				'cat1' => $result->cat1,
//				'total' => $totalScore,
				'totalOver100' => $totalOver100,
				'letterGrade' => $letterGrade,
				'remarks' => $remark,
				'targetStatus' => $targetStatus

			);


			$subject['result'] = $resultItem;

		}


		return view("dashboard.staff.results.endoftermSS3.template",[
			'student' => $student,
			'subjects' => $subjects,
			'target' => $target,
			'highestClassAverage' => $highestClassAverage

		]);

	}
	public function endOfTermResult() {

		$sid = session()->get('student')->sid;
		$student = student::find($sid);

		$subids = array();

		$subjectResults = subjectResult::where('sid',$student->sid)->get();

		foreach($subjectResults as $subjectResult){
			array_push($subids,$subjectResult->subid);
		}
		$subjects = subjects::whereIn("subid",$subids)->get();

		//get batch number
		if(result::where('cid',$student->cid)->count() > 0){
			$batch = result::where('cid',$student->cid)->get()->last()->batch;
			if(!isset($batch) || empty($batch)) $batch = 0;
		} else $batch = 0;

		$highestClassAverage = result::where('cid',$student->cid)->where('batch',$batch)->orderBy('average','desc')->get()->first()->average;

//		$highestClassAverage = result::where('cid',$student->cid)->orderBy('average','desc')->get()->first()->average;
		$target = 60;

		foreach($subjects as $subject){
			$result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->get()->last();

			$cat1 = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1;
			$cat2 = $result->ce3 + $result->ce4 + $result->as2 + $result->cat2;
			$exam = $result->exam;
			$project = $result->pr;

			$totalOver100 = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1 + $result->ce3 + $result->ce4 + $result->as2 + $result->cat2 + $result->pr + $result->exam;

			$letterGrade = '';
			$remark = '';
			$targetStatus = $totalOver100 - $target;

			switch ($totalOver100){
				case $totalOver100 <= 49:
					$letterGrade = 'F';
					$remark = 'FAIL';
					break;
				case $totalOver100 >= 50 && $totalOver100 < 60:
					$letterGrade = 'E';
					$remark = 'PROBATION';
					break;
				case $totalOver100 >= 60 && $totalOver100 < 70:
					$letterGrade = 'C';
					$remark = 'CREDIT';
					break;
				case $totalOver100 >= 70 && $totalOver100 < 80:
					$letterGrade = 'B';
					$remark = 'GOOD';
					break;
				case $totalOver100 >= 80 && $totalOver100 < 90:
					$letterGrade = 'B+';
					$remark = 'VERY GOOD';
					break;
				case $totalOver100 >= 90 && $totalOver100 <= 100:
					$letterGrade = 'A';
					$remark = 'EXCELLENT';

			}

			$resultItem = array(
				'ca1' => $cat1,
				'ca2' => $cat2,
				'pr' => $project,
				'exam' => $exam,
				'totalOver100' => $totalOver100,
				'letterGrade' => $letterGrade,
				'remarks' => $remark,
				'targetStatus' => $targetStatus

			);


			$subject['result'] = $resultItem;

		}


		return view("dashboard.staff.results.endofterm.template",[
			'student' => $student,
			'subjects' => $subjects,
			'target' => $target,
			'highestClassAverage' => $highestClassAverage

		]);
	}


	public function sid() {
		if(session()->has('student')) $sid = session()->get('student')->sid;
		else
			return redirect("student/login");
		return $sid;
	}

	public static function checkLogin(){
		if (!session()->has('student')) return redirect('student/login'); //check login
	}

	public function tests() {

		if (session()->get('student') == null) return redirect('student/login'); //check login

		$cid = session()->get('student')->cid;
		$sid = $this->sid();


		$testClass = testClass::where('cid',$cid)->get();
		$testIDs = array();

		foreach($testClass as $item){
			$test =  test::find($item->testid);
			if(count($test->Questions) > 0 ) // check if there are questions in the test
			array_push($testIDs, $item->testid);
		}

		$tests = test::whereIn('testid',$testIDs)->where('endTime','>=',Carbon::now())->get();

		$modifiedTests = array();

		foreach($tests as $test){

			$testResult = testResult::where('testid',$test->testid)->where('sid',$sid)->get()->last();
			if(isset($testResult))
				$test = collect($test)->put("attempt",$testResult->attempt)->put("score",$testResult->score)->put("total",$testResult->total);

			array_push($modifiedTests,$test);

		}

		return view('dashboard.student.tests.manage',[
			'tests' => $tests,
			'modifiedTests' => $modifiedTests
		]);
	}



	public function subjects() {
		$sid = session()->get('student')->sid;
		$subids = array();

		$student = student::find($sid);

//		foreach(subjectStudent::where('sid',$sid)->get() as $ss){
//			array_push($subids, $ss->subid);
//		}
//
//
//		$subjects = subjects::whereIn('subid',$subids)->get();

		return view('dashboard.student.subjects.manage',[
			'subjects' => $student->Subjects
		]);
	}

	public function subjectDetails($subid) {


		if (!session()->has('student')) return redirect('student/login'); //check login

		$subject = subjects::find($subid);

		$cid = session()->get('student')->cid;

		$sid = session()->get('student')->sid;


		$testClass = testClass::where('cid',$cid)->get();
		$testIDs = array();

		foreach($testClass as $item){
			array_push($testIDs, $item->testid);
		}

		$tests = test::whereIn('testid',$testIDs)->where('subid',$subid)->get();

		$modifiedTests = array();

		foreach($tests as $test){

			$testResult = testResult::where('testid',$test->testid)->where('sid',$sid)->get()->last();
			if(isset($testResult))
			$test = collect($test)->put("attempt",$testResult->attempt)->put("score",$testResult->score)->put("total",$testResult->total);

			array_push($modifiedTests,$test);
		}


		return view('dashboard.student.subjects.details',[
			'subject' => $subject,
			'tests' => $tests,
			'modifiedTests' => $modifiedTests
		]);
	}


	public function takeTest( $testid ) {
		$test = test::find($testid);

		$questionsArray = array();


		foreach($test->Questions as $question){
			$questionItem = array();
			$questionItem['qid'] = $question->qid;
			$questionItem['question'] = $question->question;
			$questionItem['image'] = $question->image;
			$questionItem['options'] = array(
				$question->option1,
				$question->option2,
				$question->option3,
				$question->option4,
				$question->option5,
			);

			array_push($questionsArray,$questionItem);
		}


		return view('dashboard.student.tests.take', [
			'test' => $test,
			'questions' => json_encode($questionsArray)
		]);
	}

    public function testHistory() {
        $student = session()->get('student');

        $responses = response::where('sid',$student->sid)->get();

        $testIdsForResponses = [];

        foreach($responses as $response){
            array_push($testIdsForResponses, $response->Question->testid );
        }

        $testIdsTaken = array_unique($testIdsForResponses);

        $testsTaken = test::whereIn('testid',$testIdsTaken)->orderBy('created_at','desc')->paginate(20);


        $modifiedTests = array();

        foreach($testsTaken as $test){

            $testResult = testResult::where('testid',$test->testid)->where('sid',$student->sid)->get()->last();
            if(isset($testResult))
                $test = collect($test)->put("attempt",$testResult->attempt)->put("score",$testResult->score)->put("total",$testResult->total);

            array_push($modifiedTests,$test);

        }


        return view('dashboard.student.tests.history',[
            'allTests' => count($testsTaken),
            'tests' => $testsTaken,
            'from' => collect($testsTaken)->get('from'),
            'modifiedTests' => $modifiedTests,
        ]);


	}

    public function testCorrection($testid) {
	    $test = test::find($testid);
        $student = session()->get('student');


	    $qids = [];

	    foreach($test->Questions as $question){
	        array_push($qids,$question->qid);
        }

	    $answers = response::whereIn('qid',$qids)->where('sid',$student->sid)->get();

	    return view('dashboard.student.tests.correction',[
	        'test' => $test,
            'answer' => $answers
        ]);

	}

	public function assignments() {
		$sid = session()->get('student')->sid;
		$subids = array();


		foreach(subjectStudent::where('sid',$sid)->get() as $ss){
			array_push($subids, $ss->subid);
		}

        $term = setting::where('name', 'term')->get()->last();

		$assignments = assignment::orderBy('created_at','desc')->where('created_at','>',$term->created_at)->where('cid', session()->get('student')->cid)->get();

		return view('dashboard.student.assignments.manage',[
			'assignments' => $assignments
		]);

	}

	public function assignmentDetails( $aid ) {
		$assignment = assignment::find($aid);

		return view('dashboard.student.assignments.details',[
			'assignment' => $assignment
		]);
	}

	public function submitAssignment( Request $request, $aid ) {

		if($request->hasFile('assignment')){
			$inputFileName = $request->file('assignment')->getClientOriginalName();
			$request->file('assignment')->move('uploads/assignments/',$inputFileName);
			$url = url('uploads/assignments/' . $inputFileName);

			$assignmentSubmission = new assignmentSubmission();
			$assignmentSubmission->aid = $aid;
			$assignmentSubmission->sid = $this->sid();
			$assignmentSubmission->url = $url;
			$assignmentSubmission->save();

			session()->flash('success','Assignment Submitted');

		} else {
			session()->flash('error','No file attached. Please upload a file');
		}

		return redirect()->back();

	}

}
