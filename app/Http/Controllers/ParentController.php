<?php

namespace App\Http\Controllers;

use App\assignment;
use App\complain;
use App\news;
use App\parents;
use App\result;
use App\setting;
use App\sickbay;
use App\staff;
use App\student;
use App\subjectResult;
use App\subjects;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ParentController extends Controller
{

	public function __construct() {
		$this->middleware('parents');
	}


	public function dashboard(  ) {
		if (session()->has('parent')):
			$parent = session()->get('parent');
		else:
			return redirect('parent/login');
		endif;

		$students = $parent->Students;
		$complaints = complain::where('pid',$parent->pid)->get();
		$news = news::where('created_at','>',Carbon::now()->subDays(14))->get();

		return view('dashboard.parent.dashboard',[
			'parent' => $parent,
			'students' => $students,
			'complaints' => $complaints,
			'news' => $news
		]);

	}

	public function news() {

		$news = news::orderBy('created_at','desc')->paginate(10);

		$from = collect($news)->get('from');

		return view('dashboard.parent.news.manage',[
			'newsItems' => $news,
			'from' => $from
		]);
	}

	public function newsDetails( $newsid ) {
		// check login
		if (!session()->has('parent')) return redirect('parent/login');

		$news = news::find($newsid);

		return view('dashboard.parent.news.details',[
			'news' => $news
		]);
	}



	public function children() {

		$pid = $this->pid();

		$parent = parents::find($pid);
		$students = $parent->Students;

		return view('dashboard.parent.child.manage',[
			'students' => $students
			]);
	}


	public function childDetails($sid) {

		$child = student::findorfail($sid);
		$health_record = sickbay::where('sid',$sid)->get();

		$assignments = assignment::where('cid',$child->cid)->whereIn('subid',$child->Subjects)->get();
		return view('dashboard.parent.child.detail',[
			'student' => $child,
			'health_record' =>$health_record,
			'assignments' => $assignments

		]);

	}


	public function childResult($sid) {

        $term = setting::where('name','term')->get()->last()->value;
        $session = setting::where('name','session')->get()->last()->value;

        $student = student::find($sid);

        $subids = array();

        $subjectResults = subjectResult::where('sid',$student->sid)->where('term',$term)->where('session',$session)->get();

        foreach($subjectResults as $subjectResult){
            array_push($subids,$subjectResult->subid);
        }
        $subjects = subjects::whereIn("subid",$subids)->get();

        //get batch number
        if(result::where('cid',$student->cid)->count() > 0){
            $batch = result::where('cid',$student->cid)->get()->last()->batch;
            if(!isset($batch) || empty($batch)) $batch = 0;
        } else $batch = 0;


        $highestClassAverage = result::where('cid',$student->cid)->where('batch',$batch)->orderBy('average','desc')->get()->first()->average;

        $target = 60;

        foreach($subjects as $subject){
            $result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->where('term',$term)->where('session',$session)->get()->last();

            if(!empty($result)) {

                $totalScore = number_format( $result->ce1 + $result->ce2 + $result->as1 + $result->cat1,2);
                $total = 5 + 5 + 5 + 10;
                $totalOver100 = ($totalScore / $total) * 100;
                $letterGrade = '';
                $remark = '';
                $targetStatus = number_format( $totalOver100 - $target ,2);

                switch ($totalOver100) {
                    case $totalOver100 < 50:
                        $letterGrade = 'F';
                        $remark = 'FAIL';
                        break;
                    case $totalOver100 >= 50 && $totalOver100 < 60:
                        $letterGrade = 'E';
                        $remark = 'PROBATION';
                        break;
                    case $totalOver100 >= 60 && $totalOver100 < 70:
                        $letterGrade = 'C';
                        $remark = 'CREDIT';
                        break;
                    case $totalOver100 >= 70 && $totalOver100 < 80:
                        $letterGrade = 'B';
                        $remark = 'GOOD';
                        break;
                    case $totalOver100 >= 80 && $totalOver100 < 90:
                        $letterGrade = 'B+';
                        $remark = 'VERY GOOD';
                        break;
                    case $totalOver100 >= 90 && $totalOver100 <= 100:
                        $letterGrade = 'A';
                        $remark = 'EXCELLENT';

                }

                $resultItem = array(
                    'ce1' => $result->ce1,
                    'ce2' => $result->ce2,
                    'as1' => $result->as1,
                    'cat1' => $result->cat1,
                    'total' => $totalScore,
                    'totalOver100' => $totalOver100,
                    'letterGrade' => $letterGrade,
                    'remarks' => $remark,
                    'targetStatus' => $targetStatus

                );


                $subject['result'] = $resultItem;

            }
        }


        return view("dashboard.staff.results.midterm.template",[
            'student' => $student,
            'subjects' => $subjects,
            'target' => $target,
            'highestClassAverage' => $highestClassAverage

        ]);

//		$student = student::find($sid);
//
//		$subids = array();
//
//		$subjectResults = subjectResult::where('sid',$student->sid)->get();
//
//		foreach($subjectResults as $subjectResult){
//			array_push($subids,$subjectResult->subid);
//		}
//		$subjects = subjects::whereIn("subid",$subids)->get();
//
//		$highestClassAverage = result::where('cid',$student->cid)->orderBy('average','desc')->get()->first()->average;
//		$target = 60;
//
//		foreach($subjects as $subject){
//			$result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->get()->last();
//
//			$totalScore = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1;
//			$total = 5 + 5 + 5 + 10;
//			$totalOver100 =  ($totalScore / $total ) * 100;
//			$letterGrade = '';
//			$remark = '';
//			$targetStatus = $totalOver100 - $target;
//
//			switch ($totalOver100){
//				case $totalOver100 <= 49:
//					$letterGrade = 'F';
//					$remark = 'FAIL';
//					break;
//				case $totalOver100 >= 50 && $totalOver100 < 60:
//					$letterGrade = 'E';
//					$remark = 'PROBATION';
//					break;
//				case $totalOver100 >= 60 && $totalOver100 < 70:
//					$letterGrade = 'C';
//					$remark = 'CREDIT';
//					break;
//				case $totalOver100 >= 70 && $totalOver100 < 80:
//					$letterGrade = 'B';
//					$remark = 'GOOD';
//					break;
//				case $totalOver100 >= 80 && $totalOver100 < 90:
//					$letterGrade = 'B+';
//					$remark = 'VERY GOOD';
//					break;
//				case $totalOver100 >= 90 && $totalOver100 <= 100:
//					$letterGrade = 'A';
//					$remark = 'EXCELLENT';
//
//			}
//
//			$resultItem = array(
//				'ce1' => $result->ce1,
//				'ce2' => $result->ce2,
//				'as1' => $result->as1,
//				'cat1' => $result->cat1,
//				'total' => $totalScore,
//				'totalOver100' => $totalOver100,
//				'letterGrade' => $letterGrade,
//				'remarks' => $remark,
//				'targetStatus' => $targetStatus
//
//			);
//
//
//			$subject['result'] = $resultItem;
//
//		}
//
//
//		return view("dashboard.staff.results.midterm.template",[
//			'student' => $student,
//			'subjects' => $subjects,
//			'target' => $target,
//			'highestClassAverage' => $highestClassAverage
//
//		]);

	}

	public function childEndOfTermResultSS3( $sid ) {

		$student = student::find($sid);

		$subids = array();

		$subjectResults = subjectResult::where('sid',$student->sid)->get();

		foreach($subjectResults as $subjectResult){
			array_push($subids,$subjectResult->subid);
		}
		$subjects = subjects::whereIn("subid",$subids)->get();

		//get batch number
		if(result::where('cid',$student->cid)->count() > 0){
			$batch = result::where('cid',$student->cid)->get()->last()->batch;
			if(!isset($batch) || empty($batch)) $batch = 0;
		} else $batch = 0;


		$highestClassAverage = result::where('cid',$student->cid)->where('batch',$batch)->orderBy('average','desc')->get()->first()->average;

//		$highestClassAverage = result::where('cid',$student->cid)->orderBy('average','desc')->get()->first()->average;
		$target = 60;

		foreach($subjects as $subject){
			$result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->get()->last();

//			$totalScore = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1;
//			$total = 5 + 5 + 5 + 10;
			$totalOver100 =  ($result->exam / 40 ) * 100;
			$letterGrade = '';
			$remark = '';
			$targetStatus = $totalOver100 - $target;

			switch ($totalOver100){
				case $totalOver100 < 50:
					$letterGrade = 'F';
					$remark = 'FAIL';
					break;
				case $totalOver100 >= 50 && $totalOver100 < 60:
					$letterGrade = 'E';
					$remark = 'PROBATION';
					break;
				case $totalOver100 >= 60 && $totalOver100 < 70:
					$letterGrade = 'C';
					$remark = 'CREDIT';
					break;
				case $totalOver100 >= 70 && $totalOver100 < 80:
					$letterGrade = 'B';
					$remark = 'GOOD';
					break;
				case $totalOver100 >= 80 && $totalOver100 < 90:
					$letterGrade = 'B+';
					$remark = 'VERY GOOD';
					break;
				case $totalOver100 >= 90 && $totalOver100 <= 100:
					$letterGrade = 'A';
					$remark = 'EXCELLENT';

			}

			$resultItem = array(
//				'ce1' => $result->ce1,
//				'ce2' => $result->ce2,
//				'as1' => $result->as1,
//				'cat1' => $result->cat1,
//				'total' => $totalScore,
				'totalOver100' => $totalOver100,
				'letterGrade' => $letterGrade,
				'remarks' => $remark,
				'targetStatus' => $targetStatus

			);


			$subject['result'] = $resultItem;

		}


		return view("dashboard.staff.results.endoftermSS3.template",[
			'student' => $student,
			'subjects' => $subjects,
			'target' => $target,
			'highestClassAverage' => $highestClassAverage

		]);

	}

	public function childEndOfTermResult($sid) {
		$student = student::find($sid);

		$subids = array();

		$subjectResults = subjectResult::where('sid',$student->sid)->get();

		foreach($subjectResults as $subjectResult){
			array_push($subids,$subjectResult->subid);
		}
		$subjects = subjects::whereIn("subid",$subids)->get();

		//get batch number
		if(result::where('cid',$student->cid)->count() > 0){
			$batch = result::where('cid',$student->cid)->get()->last()->batch;
			if(!isset($batch) || empty($batch)) $batch = 0;
		} else $batch = 0;

		$highestClassAverage = result::where('cid',$student->cid)->where('batch',$batch)->orderBy('average','desc')->get()->first()->average;

//		$highestClassAverage = result::where('cid',$student->cid)->orderBy('average','desc')->get()->first()->average;
		$target = 60;

		foreach($subjects as $subject){
			$result = subjectResult::where('sid',$student->sid)->where('subid',$subject->subid)->get()->last();

			$cat1 = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1;
			$cat2 = $result->ce3 + $result->ce4 + $result->as2 + $result->cat2;
			$exam = $result->exam;
			$project = $result->pr;

			$totalOver100 = $result->ce1 + $result->ce2 + $result->as1 + $result->cat1 + $result->ce3 + $result->ce4 + $result->as2 + $result->cat2 + $result->pr + $result->exam;

			$letterGrade = '';
			$remark = '';
			$targetStatus = $totalOver100 - $target;

			switch ($totalOver100){
				case $totalOver100 <= 49:
					$letterGrade = 'F';
					$remark = 'FAIL';
					break;
				case $totalOver100 >= 50 && $totalOver100 < 60:
					$letterGrade = 'E';
					$remark = 'PROBATION';
					break;
				case $totalOver100 >= 60 && $totalOver100 < 70:
					$letterGrade = 'C';
					$remark = 'CREDIT';
					break;
				case $totalOver100 >= 70 && $totalOver100 < 80:
					$letterGrade = 'B';
					$remark = 'GOOD';
					break;
				case $totalOver100 >= 80 && $totalOver100 < 90:
					$letterGrade = 'B+';
					$remark = 'VERY GOOD';
					break;
				case $totalOver100 >= 90 && $totalOver100 <= 100:
					$letterGrade = 'A';
					$remark = 'EXCELLENT';

			}

			$resultItem = array(
				'ca1' => $cat1,
				'ca2' => $cat2,
				'pr' => $project,
				'exam' => $exam,
				'totalOver100' => $totalOver100,
				'letterGrade' => $letterGrade,
				'remarks' => $remark,
				'targetStatus' => $targetStatus

			);


			$subject['result'] = $resultItem;

		}


		return view("dashboard.staff.results.endofterm.template",[
			'student' => $student,
			'subjects' => $subjects,
			'target' => $target,
			'highestClassAverage' => $highestClassAverage

		]);
	}


	public function addComplaint() {

		$pid = $this->pid();

		$parent = parents::find($pid);
		$students = $parent->Students;

		$staffs = staff::where('category','Academic')->get();

		return view('dashboard.parent.complaints.add',[
			'students' => $students,
			'staffs'   => $staffs
		]);
	}

	public function postAddComplaint(Request $request ) {

		$pid = session()->get('parent')->pid;
		$sid = $request->input('sid');
		$stid = $request->input('stid');


		$complain = new complain();
		$complain->complaint = $request->input('content');
		$complain->pid = $pid;
		$complain->sid = $sid;
		$complain->stid = $stid;
		$complain->isPrincipalViewed = 0;
		$complain->isAdminViewed = 0;

		$status = $complain->save();

		if ($status){
			session()->flash('success','Complain Sent');
		}else{
				session()->flash('error','Something Went Wrong');
			}

			return redirect()->back();

	}

	public function complaint( $comid ) {
		$complaint = complain::find($comid);
		return view('dashboard.parent.complaints.details',[
			'complaint' => $complaint
		]);
	}

	public function complaints() {
		$pid = $this->pid();

		$complaints = complain::where('pid',$pid)->get();

		return view('dashboard.parent.complaints.manage',[
			'complaints' => $complaints
		]);
	}


	public function pid() {
		if(session()->has('parent')) $pid = session()->get('parent')->pid;
		else
			return redirect("parent/login");
		return $pid;
	}


	public function checkLogin() {
		if (session()->has('parent')):
			$parent = session()->get('parent');
		else:
			return redirect('parent/login');
		endif;
	}



}
