<?php

namespace App\Http\Controllers;

use App\Application;
use App\parent_student;
use App\parents;
use App\parentStudent;
use App\student;
use Faker\Provider\Company;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;

class AdmissionController extends Controller
{


	public function __construct()
	{
		$this->middleware('auth');
	}


	public function addApplication() {
		return view('admission.add');
	}

	public function editApplication($apid) {
		$application = Application::find($apid);
		return view('admission.edit',[
			'application' => $application
		]);
	}

	public function uploadApplication() {
		return view('admission.upload');
	}

	public function postUploadApplications( Request $request ) {

		if($request->hasFile('file')){
			$fileName = Carbon::now()->timestamp . $request->file('file')->getClientOriginalName();
			$request->file('file')->move('uploads/applications',$fileName);
			$fileUrl = url('uploads/applications/' . $fileName);
		} else{
			session()->flash('error','Please attach a file.');
			return redirect()->back();
		}



		try {

			$downloadUrl = url("/uploads/applications/".$fileName);

			/* Identify file, create reader and load file  */
			$inputFileType = PHPExcel_IOFactory::identify( getcwd()."/" . "uploads/applications/".$fileName);
			$objReader = PHPExcel_IOFactory::createReader($inputFileType);
			$objPHPExcel = PHPExcel_IOFactory::load(getcwd()."/" . "uploads/applications/".$fileName);

			$sheet = $objPHPExcel->getActiveSheet();
			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();
			$time_pre = microtime(true);

			//  Read a row of data into an array
			$rowData = $sheet->rangeToArray('A2:' . $highestColumn . $highestRow,
				NULL, FALSE, FALSE);

			$count = 0;

			$failedImports = array();

			// add results to data base from file
			foreach($rowData as $cell) {
				$count++;
				try {


					if(!empty($cell[0]) && !empty($cell[1]) && !empty($cell[2]) ) {
						$application = new Application();

						if ( session()->has( 'staff' ) ) {
							$application->uid       = session()->get( 'staff' )->stid;
							$application->staffRole = "Staff";
						}
						if ( auth()->check() ) {
							$application->uid       = auth()->user()->uid;
							$application->staffRole = "Admin";
						}


						$application->surName       = $cell[0];
						$application->foreNames     = $cell[1];
						$application->preferredName = $cell[2];

						$date        = "";
						$dateCell    = $objPHPExcel->getActiveSheet()->getCell( 'D' . ( $count + 1 ) );
						$invalidDate = $objPHPExcel->getActiveSheet()->getCell( 'D' . ( $count + 1 ) )->getValue();
						if ( PHPExcel_Shared_Date::isDateTime( $dateCell ) ) {
							$date = date( "d-m-Y", PHPExcel_Shared_Date::ExcelToPHP( $invalidDate ) );
						}

						$application->dob                  = $date;
						$application->gender               = $cell[4];
						$application->nationality          = $cell[5];
						$application->religionDenomination = $cell[6];
						$application->homeAddress          = $cell[7];
						$application->homePhone            = $cell[8];
						$application->firstLanguage        = $cell[9];
						$application->proposedYear         = $cell[10];

						$application->currentSchool    = $cell[11];
						$application->headTeacher      = $cell[12];
						$application->headTeacherEmail = strtolower( $cell[13] );
						$application->schoolPhone      = $cell[14];


						$application->father             = $cell[15];
						$application->fatherTitle        = $cell[16];
						$application->fatherProfession   = $cell[17];
						$application->fatherIndustry     = $cell[18];
						$application->fatherEmployerName = $cell[19];
						$application->fatherPhone        = $cell[20];
						$application->fatherEmail        = strtolower( $cell[21] );


						$application->mother             = $cell[22];
						$application->motherTitle        = $cell[23];
						$application->motherProfession   = $cell[24];
						$application->motherEmployerName = $cell[25];
						$application->motherPhone        = $cell[26];
						$application->motherEmail        = strtolower( $cell[27] );


						$application->parentsName    = $cell[29];
						$application->parentsAddress = $cell[30];
						$application->hearOfHendon   = $cell[31];

						//other people with parental responsibility


						$application->others              = $cell[32]; //create
						$application->fname               = $cell[33];
						$application->sname               = $cell[34];
						$application->address             = $cell[35];
						$application->email               = strtolower( $cell[36] );
						$application->phone               = $cell[37];
						$application->relationshipToChild = $cell[38];
						$application->medicalConditions   = $cell[39];
						$application->status              = 'pending';

						$application->save();
					}


				}

				catch(\Exception $e){
//					return $e->getMessage();
					array_push($failedImports,$count );
				}

			}

			if(count($failedImports)>0){
				$error = "";
				foreach($failedImports as $item){
					$error .= ",$item";
				}

				$failed = count($failedImports);
				session()->flash('error',"$failed did not import. [$error] ");
			}

			$number = $count - count($failedImports);
			$request->session()->flash('success',"$count records in total. $number uploaded.");
			return redirect()->back();

		}
		catch (\Exception $e) {
			die('Error loading file "' . pathinfo($fileName, PATHINFO_BASENAME)
			    . '": ' . $e->getMessage());
		}


	}

	public function postApplication(Request $request ) {


		$application = new Application();
		$application->surname = $request->input('surName');
		$application->forenames = $request->input('foreNames');
		$application->preferredName = $request->input('preferredName');
		$application->dob = $request->input('dob');
		$application->gender = $request->input('gender');
		$application->nationality = $request->input('nationality');
		$application->religionDenomination = $request->input('religionDenomination');
		$application->homeAddress = $request->input('homeAddress');
		$application->homePhone = $request->input('homePhone');
		$application->firstLanguage = $request->input('firstLanguage');
		$application->proposedYear = $request->input('proposedYear');
		$application->connection = $request->input('connection');
		$application->accommodation = $request->input('accommodation');


		$application->currentSchool = $request->input('currentSchool');
		$application->attendanceDate = $request->input('attendanceDate');
		$application->headTeacher = $request->input('headTeacher');
		$application->headTeacherEmail = strtolower($request->input('headTeacherEmail'));
		$application->schoolPhone = $request->input('schoolPhone');
		$application->hearOfHendon = $request->input('hearOfHendon');




		$application->father = $request->input('father');
		$application->fatherTitle = $request->input('fatherTitle');
		$application->fatherProfession = $request->input('fatherProfession');
		$application->fatherIndustry = $request->input('fatherIndustry');
		$application->fatherEmployerName = $request->input('fatherEmployerName');
		$application->fatherEmail = strtolower($request->input('fatherEmail'));
		$application->fatherPhone = $request->input('fatherPhone');


		$application->mother = $request->input('mother');
		$application->motherTitle = $request->input('motherTitle');
		$application->motherProfession = $request->input('motherProfession');
		$application->motherIndustry  = $request->input('motherIndustry');
		$application->motherEmployerName = $request->input('motherEmployerName');
		$application->motherEmail = strtolower($request->input('motherEmail'));
		$application->motherPhone = $request->input('motherPhone');



		$application->fname = $request->input('fname');
		$application->sname = $request->input('sname');
		$application->address = $request->input('address');
		$application->email = strtolower($request->input('email'));
		$application->phone = $request->input('phone');
		$application->relationshipToChild = $request->input('relationshipToChild');
		$application->status = 'pending';

		$status = $application->save();

		if ($status){
			session()->flash('success','Application Created.');
		}else{
			session()->flash('error','Something went Wrong, Try again');
		}

		return redirect()->back();

	}

	public function postEditApplication( Request $request , $apid) {


		$application = Application::find($apid);
		$application->surname = $request->input('surName');
		$application->forenames = $request->input('foreNames');
		$application->preferredName = $request->input('preferredName');
		$application->dob = $request->input('dob');
		$application->gender = $request->input('gender');
		$application->nationality = $request->input('nationality');
		$application->religionDenomination = $request->input('religionDenomination');
		$application->homeAddress = $request->input('homeAddress');
		$application->homePhone = $request->input('homePhone');
		$application->firstLanguage = $request->input('firstLanguage');
		$application->proposedYear = $request->input('proposedYear');
		$application->connection = $request->input('connection');
		$application->accommodation = $request->input('accommodation');


		$application->currentSchool = $request->input('currentSchool');
		$application->attendanceDate = $request->input('attendanceDate');
		$application->headTeacher = $request->input('headTeacher');
		$application->headTeacherEmail = strtolower($request->input('headTeacherEmail'));
		$application->schoolPhone = $request->input('schoolPhone');
		$application->hearOfHendon = $request->input('hearOfHendon');




		$application->father = $request->input('father');
		$application->fatherTitle = $request->input('fatherTitle');
		$application->fatherProfession = $request->input('fatherProfession');
		$application->fatherIndustry = $request->input('fatherIndustry');
		$application->fatherEmployerName = $request->input('fatherEmployerName');
		$application->fatherEmail = strtolower($request->input('fatherEmail'));
		$application->fatherPhone = $request->input('fatherPhone');


		$application->mother = $request->input('mother');
		$application->motherTitle = $request->input('motherTitle');
		$application->motherProfession = $request->input('motherProfession');
		$application->motherIndustry  = $request->input('motherIndustry');
		$application->motherEmployerName = $request->input('motherEmployerName');
		$application->motherEmail = strtolower($request->input('motherEmail'));
		$application->motherPhone = $request->input('motherPhone');



		$application->fname = $request->input('fname');
		$application->sname = $request->input('sname');
		$application->address = $request->input('address');
		$application->email = strtolower($request->input('email'));
		$application->phone = $request->input('phone');
		$application->relationshipToChild = $request->input('relationshipToChild');
		$application->status = 'pending';

		$application->save();

		session()->flash('success','Application Edited.');
		return redirect()->back();

	}


	public function getApplications() {

		if (Input::has('name')){
			$term = Input::get('name');

			$applications = Application::where('status',"<>","admitted")->where('surname' ,'like',"%$term%")->orWhere('forenames' ,'like',"%$term%")->orderBy('created_at','desc')->paginate(20);
		}else{
			$applications = Application::where('status','pending')->orderBy('created_at','desc')->paginate(20);

		}
		$allApplications = Application::all();
		$pendingApplications = Application::where('status','pending')->get();

		$from =  collect($applications)->get('from');
		return view('admission.manage',[
			'allApplications' => $allApplications,
			'pendingApplications' => $pendingApplications,
			'applications' => $applications,
			'from' => $from
		]);
	}
	


	public function applicationDetail($apid) {

		$application   = Application::find($apid);
		return view('admission.detail',[
			'application' => $application
		]);
	}

	public function deleteApplication( $apid ) {
		Application::destroy($apid);
		session()->flash('success','Application Deleted');
		return redirect()->back();
	}



	public function makeStudent($apid) {


		try{

			DB::beginTransaction();
				$application = Application::find($apid);
				$application->status = 'admitted';
				$application->save();

				$fnameInital =  substr($application->forenames,0,1);
				$sname =  str_slug($application->surname);
				$email = strtolower( $fnameInital . ".". $sname . "@hendoncollege.org");

				while(student::where('email',$email)->count() > 0){
					$number = rand(0,100);
					$email = strtolower( $fnameInital . ".". $sname . $number . "@hendoncollege.org");
				}


				$student              = new student();
				$student->fname       = $application->forenames;
				$student->sname       = $application->surname;
				$student->dob         = $application->dob;
				$student->phone       = $application->homePhone;
				$student->gender      = $application->gender;
				$student->email       = $email;
				$student->address     = $application->homeAddress;
				$student->religion    = $application->religionDenomination;
				$student->language    = $application->firstLanguage;
				$student->nationality = $application->nationality;
				$student->password    = bcrypt( 'hendon123' );

				$student->save();

				try{


				$father               = new parents();
				$father->relationship = 'father';
				$father->fname        = $application->father;
				$father->phone        = $application->fatherPhone;
				$father->email        = strtolower($application->fatherEmail);
				$father->address      = $application->homeAddress;
				$father->password     = bcrypt( 'hendon123' );
				$father->save();

				$father_student      = new parentStudent();
				$father_student->pid = $father->pid;
				$father_student->sid = $student->sid;
				$father_student->save();


				}catch (\Exception $exception){}

				try{


				$mother               = new parents();
				$mother->relationship = 'mother';
				$mother->fname        = $application->mother;
				$mother->phone        = $application->motherPhone;
				$mother->email        = strtolower($application->motherEmail);
				$mother->address      = $application->homeAddress;
				$mother->password     = bcrypt( 'hendon123' );
				$mother->save();

				$mother_student      = new parentStudent();
				$mother_student->pid = $mother->pid;
				$mother_student->sid = $student->sid;
				$mother_student->save();

				}catch (\Exception $exception){}




			DB::commit();

			session()->flash('success','Successful');

			return redirect()->back();


		}catch (\Exception $exception){


			return $exception->getMessage();
			session()->flash('error',"Something went wrong. Please try again or contact IT.");

			return redirect()->back();
		}

	}

	public function admitAll() {


		try {

			$applications = Application::where( 'status', 'pending' )->get();

			foreach ( $applications as $application ) {


//				DB::beginTransaction();
				$application = Application::find($application->apid);
				$application->status = 'admitted';
				$application->save();

				$fnameInital =  substr($application->forenames,0,1);
				$sname =  str_slug($application->surname);
				$email = strtolower( $fnameInital . ".". $sname . "@hendoncollege.org");


				$student              = new student();
				$student->fname       = $application->forenames;
				$student->sname       = $application->surname;
				$student->dob         = $application->dob;
				$student->phone       = $application->homePhone;
				$student->gender      = $application->gender;
				$student->email       = $email;
				$student->address     = $application->homeAddress;
				$student->religion    = $application->religionDenomination;
				$student->language    = $application->firstLanguage;
				$student->nationality = $application->nationality;
				$student->password    = bcrypt( 'hendon123' );

				$student->save();

				try{


					$father               = new parents();
					$father->relationship = 'father';
					$father->fname        = $application->father;
					$father->phone        = $application->fatherPhone;
					$father->email        = strtolower($application->fatherEmail);
					$father->address      = $application->homeAddress;
					$father->password     = bcrypt( 'hendon123' );
					$father->save();

					$father_student      = new parentStudent();
					$father_student->pid = $father->pid;
					$father_student->sid = $student->sid;
					$father_student->save();


				}catch (\Exception $exception){}

				try{


					$mother               = new parents();
					$mother->relationship = 'mother';
					$mother->fname        = $application->mother;
					$mother->phone        = $application->motherPhone;
					$mother->email        = strtolower($application->motherEmail);
					$mother->address      = $application->homeAddress;
					$mother->password     = bcrypt( 'hendon123' );
					$mother->save();

					$mother_student      = new parentStudent();
					$mother_student->pid = $mother->pid;
					$mother_student->sid = $student->sid;
					$mother_student->save();

				}catch (\Exception $exception){}



//				DB::commit();

			}

			session()->flash('success','Successful');

			return redirect()->back();


		}catch (\Exception $exception){

			return $exception->getMessage();
			session()->flash('error',"Something went wrong. Please try again or contact IT.");

			return redirect()->back();
		}

	}
	
}
