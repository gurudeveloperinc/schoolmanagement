<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subjectTeacher extends Model
{
    protected $primaryKey = 'subtid';
    protected $table = 'subjectteachers';

	public function Subject() {
		return $this->belongsTo(subjects::class,'subid','subid');
    }

	public function Teacher() {
		return $this->belongsTo(staff::class,'stid','stid');
    }
}
