<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class student extends Model
{
    protected  $primaryKey = 'sid';
    protected  $guarded = [];


	public function Parents(  ) {
		return $this->belongsToMany(parents::class,'parentstudents','sid','pid');
    }


	public function Class() {
		return $this->belongsTo(classes::class,'cid','cid');
    }

	public function Focus(  ) {
		return $this->belongsTo(focus::class,'fid','fid');
    }


	public function Subjects() {
		return $this->belongsToMany(subjects::class,'subjectsstudents','sid','subid');
    }

	public function SubjectStudents() {
		return $this->hasMany(subjectStudent::class,'sid','sid');
    }

	public function ParentStudents() {
		return $this->belongsTo(parentStudent::class,'sid','sid');
    }



	// this is a recommended way to declare event handlers
	public static function boot() {
		parent::boot();

		static::deleting(function($student) { // before delete() method call this
			foreach($student->SubjectStudents as $item) $item->delete();
			foreach($student->ParentStudents as $item) $item->delete();

		});
	}

	public function Results() {
		return $this->hasMany(result::class,'sid','sid');
	}


}
