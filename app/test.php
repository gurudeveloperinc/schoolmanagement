<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class test extends Model
{
	protected $primaryKey = 'testid';
	protected $table = 'tests';

	protected $dates=['created_at','updated_at','deadline'];

	public function Questions() {
		return $this->hasMany(question::class,'testid','testid');
	}
	
	public function Classes() {
		return $this->belongsToMany(classes::class,'testclasses','testid','cid');
	}

	public function Staff() {
		return $this->belongsTo(staff::class,'stid','stid');
	}

	public function Subject() {
		return $this->hasOne(subjects::class,'subid','subid');
	}

	public function Attempts() {
		return $this->hasMany(attempt::class,'testid','testid');
	}

	public function Results() {
		return $this->hasMany(testResult::class,'testid','testid');
	}

}
