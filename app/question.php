<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class question extends Model
{
    protected $primaryKey = 'qid';
    protected $table = 'questions';

	public function Quiz() {
		return $this->belongsTo(test::class,'testid','testid');
    }

	public function Test() {
		return $this->belongsTo(test::class,'testid','testid');
    }

	public function Staff() {
		return $this->belongsTo(staff::class,'stid','stid');
    }

	public function Response() {
		return $this->hasMany(response::class,'qid','qid');
    }

}
