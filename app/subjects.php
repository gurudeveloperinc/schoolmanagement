<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subjects extends Model
{
    protected $primaryKey = 'subid';
    protected $guarded = [];


	public function Teacher(  ) {
		return $this->belongsTo(staff::class,'stid');
    }

	public function Teachers() {
		return $this->belongsToMany(staff::class,"subjectteachers","subid","stid");
    }

	public function Students() {
		return $this->belongsToMany(student::class,'subjectsstudents','subid','sid');
    }

	public function Tests() {
		return $this->hasMany(test::class,'subid','subid');
    }


	public function Updates() {
		return $this->hasMany(subjectUpdate::class,'subid','subid');
    }

	public function Files() {
		return $this->hasMany(subjectFile::class,'subid','subid');
    }

	public function Assignments() {
		return $this->hasMany(assignment::class,'subid','subid');
    }

	public function Results() {
		return $this->hasMany(subjectResult::class,'subid','subid');
    }
}
