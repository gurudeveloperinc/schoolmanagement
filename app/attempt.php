<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class attempt extends Model
{
    protected $primaryKey = 'atid';
    protected $table = 'attempts';

	public function Student() {
		return $this->belongsTo(student::class,'sid','sid');
    }
}
