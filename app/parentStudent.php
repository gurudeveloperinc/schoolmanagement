<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class parentStudent extends Model
{

	protected $primaryKey = 'psid';
	protected $table = 'parentstudents';
	protected $guarded = [];

}
